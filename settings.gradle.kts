@file:Suppress("SpellCheckingInspection")

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            when (requested.id.id) {
                "kotlin-multiplatform" -> {
                    useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:${requested.version}")
                }
            }
        }
    }

    repositories {
        // Kotlin Multi-platform Gradle Plugin EAP version, otherwise gradlePluginPortal()
        // EAP repo - https://dl.bintray.com/kotlin/kotlin-eap
        maven {
            setUrl( Repos.KOTLIN_EAP )
        }

        // Android Plugin for Gradle i.e. "com.android.*" is NOW published in the "google() repository, not on the
        // Gradle Plugin Portal and not on Maven Central (any longer) -- MOST CURRENT IS ON GOOGLE REPO
        // i.e. map for "com.android.*" to "com.android.tools.build" on the google() repo
        // google() = https://maven.google.com/ or https://dl.google.com/dl/android/maven2/
        // mavenCentral() = https://repo.maven.apache.org/maven2/ or http://central.maven.org/maven2/
        google() // For Gradle 4.0+
        //maven { url 'https://maven.google.com' } // For Gradle < 4.0

        mavenCentral()

        // Gradle Plugin Portal i.e. gradlePluginPortal = https://plugins.gradle.org/m2/
        gradlePluginPortal()

        // Jcenter() = https://jcenter.bintray.com
        // Gradle has JCenter built in by default, so one can simply replace mavenCentral() with jcenter()
        jcenter()
    }
}

rootProject.name = Pom.ARTIFACT_ID

enableFeaturePreview("GRADLE_METADATA")
