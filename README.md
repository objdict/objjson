# objJSON: Imposed Type, Version Safe JSON for Objects in Kotlin


> -   [API](#api).
> -   [ObjJSON: Why Imposed Types?: What is different?](#objjson-why-imposed-types).
> -   [Non-strict formatting](non-strict-formatting)
> -   [ObjJSON: Simple Example](#objjson-simple-example)
> -   [Version Safe Json for Client Server](#version-safe-json-for-client-server)
> -   [Advanced Features](#advanced-features)


#### API

> -   [From Json String to Map](#from-json-string-to-map) *String*.instanceFromJson(Bool).
> -   [From Json String to Object](#from-json-string-to-object) *Any*.instanceFromJson(Class, Bool)
> -   [From Object to Json String](#from-object-to-json-string) *Any*.objectToJson(spaces=0)
> -   [From Map to Object](#from-map-to-object) *Map*.instanceFromMap()

[//]: # "   [Solutions to Preserving Types with JSON] "


### From Json String to Map
``` 
String.instanceFromJson(selfTypes:Boolean=true):Map<String,Any>
var answer1 = """{ "abc": 3 }""".instanceFromJson()
// returns the same as
var answer2:Map<String,Any> = mutableMapOf("abc" to 3)
//while 
var noNum1 = """{ "abc": 3 }""".instanceFromJson(false)
// returns the same as
var noNum2:Map<String,Any> = mutableMapOf("abc" to "3")
// see the section 'What is imposed type?' for why 
```

"String".instanceFromJson is a String class extension function, which when 
used without a class as 
the first parameter, and without setting 'selfTypes' to false, 
simply decodes a json string into a map, using 
conventional 'self type' json types, with the exception that on some platforms
(currently JVM) the default type for numeric values with a decimal is
BigDecimal. In this form, 'instanceFromJson' is just another json parser.
There is multi-platform support and minimal requirements of the json formatting
but otherwise it is a simple json parser.
  
With 'selfTypes=false', the json parser is modified to act as a front
end for 'imposed types'.  Currently this means both integer values and values with
as decimal are returned as the string from the original json text, with 
no conversion to a numeric type taking place at this time.

A limitation of this function is that
it has a fixed return type of Map, so the
parser only functions correctly if the outer level of the Json string 
represents a map. 
Lists (e.g. """ \[1,2,3]""") at the outer level are legal Json, 
but currently this function always return a map at the outer level.

### From Json String to Object
``` 
fun <T : Any>String.instanceFromJson(cls: KClass<T>):T
class Test(val abc:Int)
var answer1 = """{ "abc": 3 }""".instanceFromJson(abc::class)
// returns the same as
var answer2 = Test(abc=3)
//as does
var answer3 = """{ abc: "3" }""".instanceFromJson(abc::class) 

// see the section 'What is imposed type?' for why 
```

"String".instanceFromJson(KClass) is a String class extension 
function, which when supplied a class (which can contain any number
of nested classes) will use the class information as a template and
instance a member of the class from the json string.

The class or class hierarchy supplied becomes the source of data for 
imposing types on the json data.

Data from the json string is converted to the actual types as
demanded by the class specification.

#### From Object to Json String
```
// fun Any.objectToJson(spaces:Int=0):String
data class Test(abc: Int)
val json = Test(abc=3).objectToJson()
print(json)
{ "abc" 3}

//  now using spaces for human reading of complex json
//  - direct from a test in repo

data class Name(val first:String,val last:String)
data class Fred(val name:Name,val age: Int)
val txt = Fred(Name("Freddy","smith"),30).objectToJson(4)
val output = """{"age": 30,
    |  "name": {"first": "Freddy",
    |      "last": "smith"
    |    }
    |}""".trimMargin()

assertEquals(output,txt, "simple from test2 fmt")
```
There is nothing particularly different in objjson than other packages
when it comes to converting Objects to json for uses such as saving or 
sending data. The innovations of objJson are in the parsing/loading of 
data from Json.
Code for processing incoming messages is more
complex than building messages to send/save. With outgoing messages, the code
only handles the message your application sends and the relevant fields
within those messages. With incoming messages, the application must
determine which message is received and deal with all possible messages,
including unexpected variations of messages.

#### From Map to Object
```
fun <T : Any> Map<String, Any?>.instanceFromMap(cls: KClass<T>): T

val message = messageString.instanceFromJson(false)
when(message["type"]){
   "special" -> val specialMessage = message.instanceFromMap(Special::class)
   else ->  val normalMessage = message.instanceFromMap(Normal::class)
}
```

This function takes a map representation of the data required to 
instance an object and all subsidiary objects, and uses that data 
to instance of object of the specified type.

The map representation is the output of the instanceFromJson(false)
function.  
The main use of this function is for when contents of a json string
need to be inspected before determining when class should be used
for full decoding.

## objJSON: Why 'Imposed Types'?
Normally Json parsing returns a map (or list), with each value in the map
having a type determined by the representation of the value. In these notes
this is referred to as 'self typed' json decoding.
'objjson'  uses an alternative approach and parses data to objects 
an 'imposed type' approach that is
the reverse of the normal 'self typed' approach to json.

To understand what is meant by 'self type' approach to json
consider the following
json string:
``` 
 """ { "name": { "First": "Fred", "Last": "Smith"}, "age":20} """
```

The type of "age" is determined by the data in the string, in this case 
the value 20 being a valid integer, determines the type for age will be
integer.
So the json data itself determines the type. Similarly, as "Fred" and 
"Smith" are clearly strings, the type of "First" and "Last" will both
by String type.  'Name' could actually be an object type for the "name", 
and one approach with 
json (as used by ObjDict) is to include information in the json specifying
type. Note, none of the quotation characters in this example would be
needed for parsing with objjson.
```  
 """ { "name": { "__type__": "Name", "first": "Fred", "last": "Smith"},
  "age":20
   "__type__": "Person" }"""
```
This '__type__' approach preserves the concept that all type information comes 
from the json string 
itself. The above example produces data that matches:
```
data class Name(val first:String="",last:String="")
data class Person(val name:Name, val age:Int = 18)
person = Person(Name("Fred", "Smith"), 30)
```

However there are definite limitations, which are all solved by 'imposed types'. 
> 1. Type Ambiguity: Basic Types
> 1. Named Type Mismatch 
> 1. Type Variations
> 1. Unexpected Type Hierarchy

1. Type Ambiguity
Even with Python, the question numerics with a decimal are 
'float' or 'Decimal'.  With Kotlin, the possibilities increase to 'float',
'double', 'Decimal' or 'BigDecimal'.  Integer values also have four 
possibilities, 'Long', 'Int', 'Short' or 'Byte'. Self typing provides no
mechanism to indicate which of these types will be expected by the application.

1. Named Type Mismatch
In the above example, consider the following data:

```  
 """ { "age": { "__type__": "Name", "first": "Fred", "last": "Smith"},
  "name": 20
   "__type__": "Person" }"""
```

This creates data with an element of named 'age' but with the 'Name' object
and 'name' field with an integer value.
Perfectly legal with json data, as in json data itself is efficetively in
kotlin terms, every JSON structure is really a `Map<String, Any>`, not
an object.  But this flexibility of JSON only allows breaking of the rules
for a static typed language where instancing a `Person` object dictates
that the value of the `name` field can **only** be a `Name` object, and the 
`Int` value of the above example is not valid if the data is a Person object.

To process such flexible JSON data, declare the information as `Map<String, Any>`,
because once declared as `Person` the types for each field are dictated by
the class, so deciding by the nature of the data what each field type should
be, is not useful.

Also, considering the example with `__type__` for the named type (as used by objJson)
when used to send
messages between host and client, a name aliasing system is required
or any change of class names will break the code unless both server and
all clients can be updated in perfect synchronisation, which is highly 
problematic for mobile client applications.

1. Type Variations
Even moving a single field from one type to another type (eg, from int
to string) can create unexpected types for a message receiver, even
if the data is still valid.  

1. Unexpected Type Hierarchies
Some applications, and even more often libraries, want the ability to process
classes in any hierarchy. However, for an application processing the specifics
of the data, such as a message, the fact that the data for a Person or 
the data for a Name can contain another Person or Name object is just 
another error case to process.  There is not logical reason in a message
to process a person inside a person unless the application expects
such a person, such as:
```
data class Person(val name:Name,val age:Int=18,val spouse:Person?=null)
```
In a statically typed language, conversion to objects *requires* that
all possible data has already been defined in the class.  Dealing with
unexpected data, will normally mean processing the data as a map, and
then using detecting type and using typecasts...which tends to be untidy.

## Non-strict formatting
Some applications desire checking json data integrity and blocking
any breaches of format rules.  Objjson takes the opposite approach.
The design goal is to allow interpreting the data whenever possible,
simplifying any manually typed json data.
This means field names or values without embedded spaces or special
characters can omit quotation marks.  When combined with imposed types,
flexibility is increased as the same characters can be translated into
different types by different applications processing the same data.

## ObjJSON: Simple Example
Consider the data from the previous example.
```
// note - the __type_- fields could be present but would be ignored
// with the data classes in this example
// also note that fields names and values that do not
// contain any spaces or special characters do not need to be'
// enclosed in quotes
val json =  """ { "name": { 
          first: "Fred", last: "Smith"
         },
         age:20 
}"""
data class Name(val first:String="",last:String="")
data class Person(val name:Name, val age:Int = 18)

val person = json.instanceFromJson(Person::class)
// now convert back to json
val newJson = person.objectToJson(spaces=4)
```

The class definitions need to provide default values for values
unless the parse of json is to fail when the value is not present.

## Version Safe JSON for Client Server
ObjJSON is a package designed to facilitate sending and receiving data
in JSON form between client and server applications. As opposed to
object serialisation, where the Objects take centre stage and the JSON
(or other serial form) is secondary, ObjJSON is designed for the case
where classes are used to construct the data to send, and on the receiver
process the data received.

Version safety is the approach of being flexible when parsing data.  
Any fields present in the json that are not in the class definition could
be fields that are defined in later releases. With the possibility this 
data may be used by a newer version, raising an error for data not in 
the definition could break the ability to deal with an updated version of
the other end of the message system. Two approaches are possible, either
a) simply ignore data outside the spec, or b) collect this data in a map
so it can be examined.

Currently this data is simply ignored, but when a use case arises, the play
is to add the option to capture this data.

 The other end of the
combinations link could be still using an older version of message
format, or may be already updated to a newer version, as it may be
impractical to update both ends of the system at once.

ObjJson is specifically designed to allow extracting the currently
needed data from a message in order to implement the currently defined
application, and allow ignoring data not relevant to the current
application. This is a different goal than systems which target imposing
the strictest possible rules on the Json data, or at the other extreme,
seek to adjust their functionality to the data.

The result is that if an message is described as, for example, containing
a person object that contains a name object, then objJson will attempt
to read that data from the message, and if data arrives describing
instead a name object at the outer level and a person object at the
inner level, there will be no attempt by objJson to adjust to this
different structure. ObjJson reads expected data only provide that data
complies with the defined object hierarchy.

Further note that when communicating between systems, even beyond the
needs of version tolerance, different systems may have variations of the
defined object properties or hierarchy and also be written in different
languages. In other words, flexibility is important.

## Advanced Features

> -   [Factory Methods](#factory-methods) 
> -   [Name Map](#name-map) 
> -   [Controlling JSON output](#controlling-json)
> -   [Interfaces](#interfaces)

### Factory Methods.
Classes have the option of providing a factory method for instancing from 
json.  This provides flexibility for different names, for the default 
constructor having another use, for validity checks, and even for 
instancing subclasses of the original class.

Simple example:

```
val json =  """ { "name": { 
          first: "Fred", last: "Smith"
         },
         age:20 
}"""
data class Name(val first:String="",last:String="")
data class Person(val fullName: String, val age:Int){
  companion object {
     fun fromJson(name: Name, age: Int = 7): Person {
     // instanceFromJson looks for factory method which must be called 'fromJson'
          return Person(name.first + " " + name.last, age)
     }
}

```
Note that it possible to include multiple overloaded factory methods for any given
object.  This adds flexibility for 'verion safety'.  If the 'old version' sends 
a single item, and the 'new version' sends a list, code that deals with both old
and new versions can be created by using two different factory methods, one with a
parameter type `List<String>`, and another with `String` and instanceFromJson with 
use the approriate factory method for the data.

A limitation with overloading is that signatures must be different, and
due to java compatibity, this means a name change for a parameter will
not be seen as a change.  
A name change between versions can best be done by adding both paramters to the factory
or the class constructor, both with defaults, usually with the new defaulting old value
when present.

## Name Map

There are times when the factory method for constructing the object is
simply overkill. When the only reason the json data is not usable
without any changes is that one (or more) of the field name in Json
does not match the corresponding variable name.  For these case
just define a `fromJsonNameMap` function in the companion object to 
map the name in
Json, to the name in the class.

```
val json =  """ { "name": "Fred Smith"
         },
         age:20 
}"""

data class Person(val fullName: String, val age:Int){
  companion object {
     fun fromJsonNameMap() = mapOf("name" to "fullName")
}

```  

### Controlling toJson
By default, objectToJson tries to write a value for every entry in the primary
constructor, except those with a default and a current value of null. (No 
need to have these in the JSON, as they default to 
null when constructed anyway) 

So if there is `class abc(val a:Int, var b:Int, c:Int)` objectToJson will
look for a value for each of `a` `b` and `c`.  Even though `c` is not automatically
a property, from the constructor, there could still be a `c` property, and if so
it will be included in the output.

Two methods are available to change the output:

```
fun objJsonExcludes():ListOf<String>
fun objJsonExtras(): MapOf<String,Any?>
```
 
The excludes is a list of names from the constructor to exclude from the Json string
and the extras should return a map of extra values for the json output.

Extras is called when the JSON output is to be output, so the map is 
dynamically generated, and can use any expression to calculate the value
of each `extra` and even dynamically decide which values are to be in the map.

Note that extras can provide entries matching those from properties that appear in
the constructor, and in this case the value(s) from extras with be the values in the 
output. 

Also Note that if a parameter is nullable and has a default other than 
null, it is best to ensure the value of that paramter is in the json
even with the current value is null but including these fields in extras 

### Interfaces

Some classes have parameters which are interfaces for added flexibility. 
Consider if in place of
a `Name` object in the examples above, the `name` parameter is uses `NameInterface`
as a type, which allows for any object which implements the name interface.

However, instancing the object from json requires using a constructor, and
interfaces do not have a constructor.  Instancing values specified by 
interface require a mapping to the actual method used to construct the
value.  This method is

```
companion object{
   def fromJsonInterfaces(): Map<String, List<Class::class>>{
       return mapOf .....
   }
}
```

Important: This method must be used to construct objects, so it must
be a companion object method and therefore be available before the
object has been created.



## Proposals:
https://discuss.kotlinlang.org/t/interface-method-in-kotlin-should-support-protected-internal-visibility-modifier/1918/13
See the example of invoke on companion as factory methods.  
Needs consideration


