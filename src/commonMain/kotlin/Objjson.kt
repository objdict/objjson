/**
 * Created by ian on 19/05/2017.
 */
package nz.salect.objjson

import kotlin.reflect.KClass

data class IterMark(val index: Int, val mark: String) {
    companion object {
        fun find(target: String, specials: List<String>, start: Int): IterMark? {
            val answer = target.findAnyOf(specials, start)
            return if (answer == null) null else IterMark(answer.first, answer.second)
        }
    }
}

fun iterMarks(strng: String, specials: List<String>, idx: Int = 0) = sequence {
    val inquotes = listOf("\\", "\"")
    var found: IterMark? //Pair<Int,String>?
    var index = idx
    do {
        found = IterMark.find(strng, specials, index)
        if (found?.mark == "\"") {
            index = found.index
            inner@ do {
                found = IterMark.find(strng, inquotes, index + 1)
                when (found?.mark) {
                    "\"" -> {
//  this is the correct code- waiting until native compiler fixed
//                      index = found.index + 1
                        index = (found?.index ?: 0) + 1
                        break@inner
                    }
//  this is the correct code- waiting until native compiler fixed
//                    "\\" -> index = found.index + 1
                    "\\" -> index = (found?.index ?: 0) + 1
                }
            } while (found != null)
        } else if (found != null) {
            yield(found)
            index = found.index + 1
        }
    } while (found != null)
}

typealias Holder = MutableMap<String, Any?>
expect class PointNum
expect fun String.toPointNum():PointNum?

//typealias ClassMap = Map<String,KClass<Any>>

class Parser(
    private val rawStr: String,
    val selfTypes:Boolean
) {
    private var lastMark: IterMark? = IterMark(-1, "") //Pair<Int,String>? = Pair(-1, "")
    private var realLastMark = IterMark(-1, "")//:Pair<Int,String> = Pair(-1, "")
    private var isDoubleBrackets: Boolean = false
    private var isDoubleBracketsEnd: Boolean = false
    private var atEmptyContainer: Boolean = false
    private var nestedDicts: MutableList<Boolean> = mutableListOf()
    private var symbols: MutableList<IterMark> = mutableListOf()

    private fun isNestDict(): Boolean {
        if (nestedDicts.isEmpty()) {
            return false
        }
        return nestedDicts.last()
    }

    private fun isNestDictEnd(): Boolean {
        if (nestedDicts.isEmpty()) {
            return false
        }
        return !nestedDicts.last()
    }

    private fun addRecentNestDict() {
        nestedDicts.add(true)
    }

    private fun removeRecentNestDict() {
        if (nestedDicts.isNotEmpty()) {
            nestedDicts.remove(nestedDicts.last())
        }
    }

    private fun closeRecentNestDict() {
        if (nestedDicts.isNotEmpty()) {
            nestedDicts[nestedDicts.lastIndex] = false
        }
    }

    private fun whiteSpace(ch: Char): Boolean {
        return " \t\n".contains(ch)
    }

    private fun consume(start: Int): Int {
        /**  this method skips over 3 symbols: , } ] that can follow closing braces */
        val res = rawStr.findAnyOf(listOf(",", "}", "]"), start + 1)
        return if (res?.second == ",") res.first + 1 else start + 1
    }

    private fun stripQuotes(fullstrng: String): String {
        if (fullstrng.isEmpty() || fullstrng.isBlank()) return ""
        val string = fullstrng.trim().dropWhile { whiteSpace(it) }
        val start = if (string[0] == '"') 1 else 0
        val end = if (string.last() == '"') 2 else 1
        return mapEscapeChars(string.slice(start..string.length - end))
    }

    private fun mapEscapeChars(str: String): String {
        var lastChar: Char? = null
        val out = mutableListOf<Char>()

        // Currently only explicitly escaping these characters
        // '\u0000' or '\0' -- null
        // '\u0007' or '\a' -- bell
        // '\u0008' or '\b' -- backspace
        // '\u0009' or '\t' -- tab
        // '\u000A' or '\n' -- line feed
        // '\u000C' or '\f' -- form feed
        // '\u000D' or '\r' -- carriage return

        // Others are not supported -- the stripped value is returned

        val chMap = mapOf(
            '0' to '\u0000',
            'a' to '\u0007',
            'b' to '\b',
            't' to '\t',
            'n' to '\n',
            'f' to '\u000C',
            'r' to '\r'
        )

        for (ch in str) {
            if (lastChar == '\\') {
                lastChar = null
                out.add(chMap[ch] ?: ch)  // was mapOrDefault ...but seems broken on js
            } else {
                if (ch != '\\')
                    out.add(ch)
                lastChar = ch
            }
        }
        return out.joinToString("")
    }

    private fun toType(instrng: String): Any? {
        val strng = instrng.trim().dropWhile { whiteSpace(it) }
        val litVals = mapOf("null" to null, "false" to false, "true" to true)
        if(selfTypes){
            val ival = strng.trim(' ').toIntOrNull()
            ival?.let { return ival }
            val pnval = strng.trim(' ').toPointNum()
            pnval?.let { return pnval }
        }

        if (strng in litVals)
            return litVals[strng]

        return stripQuotes(strng)
    }

    private fun Holder.adder(adStr: String, obj: Any? = null) {
        val key = stripQuotes(adStr.trim().split(":")[0])
        when {
            key == "" -> {
            }
            obj == null -> {
                this[key] = if (":" in adStr)
                    toType(adStr.trim().split(":", limit = 2)[1])
                else null
            }
            else -> {
                this[key] = obj
            }
        }
    }

    private fun getStart(): Int {
        // skip leading whitespace, and first '{' character
        var count = 0
        while ((count < rawStr.length) && whiteSpace(rawStr[count]))
            count++
        if (rawStr[count] == '{') count++
        return count
    }

    private fun checkDoubleBrackets(mark: IterMark?) {
        val lastMarkIndex = lastMark?.index
        if (mark == null || lastMarkIndex == null) return

        if (symbols.isEmpty() || mark.index > symbols.last().index) {
            symbols.add(mark)
        }

        if (mark.index <= lastMarkIndex) return

        if (lastMark?.mark == "[" && mark.mark == "[") {
            isDoubleBrackets = true
        } else if (lastMark?.mark == "]" && mark.mark == "]") {
            isDoubleBrackets = false
            isDoubleBracketsEnd = true
        } else if (lastMark?.mark == "[" && mark.mark == "{") {
            addRecentNestDict()
        } else if (lastMark?.mark == "," && mark.mark == "}" && isNestDict()) {
            closeRecentNestDict()
        } else if (lastMark?.mark == "}" && mark.mark == "]" && isNestDict()) {
            closeRecentNestDict()
        } else if (lastMark?.mark in listOf("{", "[") && mark.mark in listOf(
                "}",
                "]"
            ) && (lastMarkIndex) + 1 == mark.index
        ) {
            atEmptyContainer = true
        }

        lastMark = mark

        realLastMark = if (symbols.size > 1) {
            this.symbols[this.symbols.lastIndex - 1]
        } else {
            this.symbols.last()
        }
    }

    fun parse(idxIn: Int = -1): Pair<Any, Int> {
        val specials = listOf("\"", "{", "}", ",", "[")
        val hold: Holder = mutableMapOf()
        var idx = if (idxIn < 0) getStart() else idxIn
        loop@ for (mark in iterMarks(rawStr, specials, idx)) {
            checkDoubleBrackets(mark)

            if (mark == null) {

            } else if (mark.index <= idx) {
                // nothing needed if not nested list
                if (isDoubleBracketsEnd &&
                    mark.index == idx &&
                    mark.mark == "}"
                ) {
                    idx = consume(mark.index)
                    isDoubleBracketsEnd = false
                    break@loop
                } else if (isDoubleBracketsEnd &&
                    mark.index + 1 == idx &&
                    mark.mark == ","
                ) {
                    isDoubleBracketsEnd = false
                } else if (mark.index == idx &&
                    mark.mark == "}" &&
                    isNestDict() &&
                    realLastMark.mark != "]" &&
                    !atEmptyContainer &&
                    realLastMark.mark != "}"
                ) {
                    closeRecentNestDict()
                    break@loop
                } else if (atEmptyContainer && mark.index == idx) {
                    idx = consume(mark.index)
                    atEmptyContainer = false
                    break@loop
                } else if (mark.index == idx && mark.mark == "}") {
                    idx = consume(mark.index)
                    break@loop
                }
            } else {
                when (mark.mark) {
                    "}" -> {
                        if (mark.index - 1 > idx)
                            hold.adder(rawStr.slice(idx until mark.index))
                        idx = consume(mark.index)
                        break@loop
                    }
                    "[" -> {
                        val res: Pair<Any, Int> = parseLst(mark.index + 1)
                        hold.adder(rawStr.slice(idx until mark.index), res.first)
                        idx = res.second
                    }
                    "{" -> {
                        val res: Pair<Any, Int> = parse(mark.index + 1)
                        hold.adder(rawStr.slice(idx until mark.index), res.first)
                        idx = res.second
                    }

                    "," -> {
                        if (isDoubleBracketsEnd) {
                            idx = mark.index + 1
                            isDoubleBracketsEnd = false
                        } else {
                            hold.adder(rawStr.slice(idx until mark.index))
                            idx = mark.index + 1
                        }
                    }
                }

            }
        }
//        val holdType = hold.get("__type__")
//        if(holdType != null && hold!= null){
//            val data = hold as Map<*, *>
//            val holdClass = classMap[holdType]
//            if(holdClass != null) {
//                println("looking to do type")
//                val resobj = instanceFromMap(holdClass, data)
//                //?: mapOf<String,Any>()
//                return Pair(resobj,idx)
//            }
//        }
        return Pair(hold, idx)
    }

    fun MutableList<Any?>.adder(adStr: String, obj: Any? = null) {
        //val key = stripQuotes(adStr.trim().split(":")[0])
        if (obj == null) {
            if (adStr.trim(' ').matches(Regex("[\\n]+"))) {
                return
            }
            this.add(toType(adStr))
        } else {
            this.add(obj)
        }
    }

    private fun parseLst(idxIn: Int = 1): Pair<Any, Int> {
        val specials = listOf("\"", "{", "]", ",", "[")
        val lst = mutableListOf<Any?>()
        var idx = idxIn
        loop@ for (mark in iterMarks(rawStr, specials, idx)) {
            checkDoubleBrackets(mark)
            if (mark == null) continue@loop
            else if (mark.index <= idx) {
                if (isDoubleBrackets && mark.index == idx && !atEmptyContainer) {
                    val res: Pair<Any, Int> = parseLst(mark.index + 1)
                    lst.adder(rawStr.slice(idx until mark.index), res.first)
                    idx = res.second
                }

                if (isDoubleBracketsEnd && mark.index == idx) {
                    idx = consume(mark.index)
                    break@loop
                }
                if (isNestDictEnd() && mark.index == idx && mark.mark == "]") {
                    idx = consume(mark.index)
                    removeRecentNestDict()
                    break@loop
                }
                if (isNestDictEnd() && mark.index == idx) {
                    idx = consume(mark.index)
                    removeRecentNestDict()
                }
                if (isNestDict() && mark.index == idx && mark.mark == "{") {
                    val res: Pair<Any, Int> = parse(mark.index + 1)
                    lst.adder(rawStr.slice(idx..mark.index - 1), res.first)
                    idx = res.second
                }
                if (rawStr[mark.index - 1] == '[' && mark.mark == "]" && mark.index == idx) {
                    idx = consume(mark.index)
                    break@loop
                }
                if (mark.index == idx && mark.mark == "]") {
                    idx = consume(mark.index)
                    break@loop
                }
            } else {
                when (mark.mark) {
                    "]" -> {
                        if (isDoubleBracketsEnd) {
                            idx = consume(mark.index)
                            break@loop
                        }
                        if (isNestDictEnd()) {
                            val currentLast = toType(rawStr.slice(idx until mark.index)).toString()
                            if (currentLast == "}" || currentLast == "") {
                                idx = consume(mark.index)
                                removeRecentNestDict()
                                break@loop
                            }
                            lst.adder(rawStr.slice(idx until mark.index))
                            idx = consume(mark.index)
                            break@loop
                        }

                        val part = rawStr.slice(idx until mark.index)

                        if (part.isNotBlank()) {
                            lst.adder(part)
                        }

                        idx = consume(mark.index)
                        break@loop
                    }
                    "[" -> {
                        val res: Pair<Any, Int> = parseLst(mark.index + 1)
                        lst.adder(rawStr.slice(idx until mark.index), res.first)
                        idx = res.second
                    }
                    "{" -> {
                        val res: Pair<Any, Int> = parse(mark.index + 1)
                        lst.adder(rawStr.slice(idx until mark.index), res.first)
                        idx = res.second
                    }

                    "," -> {
//                        var shouldSkip = false
//
//                        if (symbols.lastIndex-3 > 0){
//                            val last = realLastmark.mark
//                            val last2nd = symbols[symbols.lastIndex-2].second
//                            val lasr3rd = symbols[symbols.lastIndex-3].second
//                            if (last == "}" && last2nd == "}" && lasr3rd == "{") {
//                                shouldSkip = true
//                            }
//                        }
//
//                        if (!shouldSkip) {
//                            lst.adder(rawStr.slice(idx until mark.index))
//                        }

                        // The real issue of this is solved by the code above, this is a shortcuts which works, but maybe failed in the future when we need more restrict condition like above
                        if (realLastMark.mark != "}") {
                            lst.adder(rawStr.slice(idx until mark.index))
                        }

                        idx = mark.index + 1
                    }
                }

            }
        }
        return Pair(lst, idx)
    }
}

@Deprecated("Use <string>.instanceFrom()  loads() was never meant to be in the interface")
fun loads(arg: String, doNums:Boolean =true): Any {
    return Parser(arg, doNums).parse().first
}

// the instanceFromJson with no paramater just returns the raw map - no top down or bottom up
fun String.instanceFromJson(doNums:Boolean=true)
    = (Parser(this, doNums).parse().first) as Map<String,Any?>?

expect fun <T : Any> Map<String, Any?>.instanceFromMap(
    cls: KClass<T>,
    raises:Boolean=false,
    other: Map<String,List<KClass<*>>>? = null
): T?

//Change signature from originstanceFromJson to instanceFromJson
fun <T : Any> String.instanceFromJson(cls: KClass<T>, raises:Boolean=false): T? {
    val res = Parser(this,selfTypes=false).parse().first
    val map = when (res) {
        is Map<*, *> -> res
        else -> null //mapOf<String, Any>()
    } as Map<String, Any?>?
    //if(cls is List)
    return map?.instanceFromMap(cls,raises)
}
