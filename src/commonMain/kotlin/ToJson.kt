package nz.salect.objjson



import kotlin.reflect.KClass

class RawStr(val str: String) {
    fun res(): String {
        return str
    }
}

fun String.jsonEscape(): String {
    // Escape the following characters
    // '\u0000' to '\\0' -- null
    // '\u0007' to '\\a' -- bell
    // '\u0008' to '\\b' -- backspace
    // '\u0009' to '\\t' -- tab
    // '\u000A' to '\\n' -- line feed
    // '\u000C' to '\\f' -- form feed
    // '\u000D' to '\\r' -- carriage return

    val out: MutableList<Char> = mutableListOf()
    for (c in this) {
        when (c) {
            '\\' -> {
                out.add('\\');out.add('\\')
            }
            '\"' -> {
                out.add('\\');out.add('\"')
            }
            in (0.toChar()..31.toChar()) -> {
                val cases = mapOf(
                    '\u0000' to '0',
                    '\u0007' to 'a',
                    '\u0008' to 'b',
                    '\u0009' to 't',
                    '\u000A' to 'n',
                    '\u000C' to 'f',
                    '\u000D' to 'r'
                )
                if (c in cases) {
                    out.add('\\')
                    out.add(cases[c] ?: ' ')
                }
            }
            else -> out.add(c)
        }
    }
    return out.joinToString(separator = "")
}
expect fun classToFields(data:Any, dumper:Dumper):String

class Dumper(private val dataAll: Any,spaces:Int =0, val dumper:Dumper?=null) {
    constructor(dataAll: Any, dumper:Dumper):this(dataAll,0,dumper)

    var depth:Int = dumper?.depth?.plus(0) ?: 0
    val spaces:Int = dumper?.spaces ?: spaces

    private fun dumpString(strng: String) = "\"" + strng.jsonEscape() + "\""

    private fun dump(data: Any): String {
        when (data) {
            null -> "foundnull"
            is String -> return dumpString(data)
            is Map<*, *> -> {
                depth++
                return dumpMap(data).also{depth--}
            }

            is Int -> return data.toString()
            is PointNum -> return data.toString()
            is Boolean -> return data.toString()
            is List<*> -> return dumpList(data)
            is RawStr -> return data.res()
            is Any -> return classToFields(data,this)

            else -> return "huh? not any of above"
        }
        return "should never get here"
    }

    private fun dumpMap(mapData: Map<*, *>): String {
        val lstStr: List<String> = mapData.toList().map {
            val key = it.first
            val dat: Any? = it.second
            val strdat = if (dat == null) "null" else dump(dat)
            "\"$key\": $strdat"
        }
        val (lineStart,commas) = if((depth == 0)||(spaces==0))
            Pair("","")
        else
            Pair("\n${" ".repeat(spaces*(depth-1))}"," ")
        return if (lstStr.isEmpty()) {
            "{}"
        } else {
            val redStr = lstStr.reduce { v1, v2 -> "$v1,$lineStart$commas $v2" }
            "{$redStr$lineStart}"
        }
    }

    private fun dumpList(lstData: List<*>): String {
        val lstStr = lstData.map {
            val dat: Any? = it
            if (dat == null) "null" else dump(dat)
        }
        var redStr = ""
        if (lstStr.isNotEmpty()) {
            redStr = lstStr.reduce { v1, v2 -> "$v1, $v2" }
        }
        return "[$redStr]"
    }

    fun dumps(): String {
        return dump(dataAll)
    }
}
@Deprecated("Use <data>.objectToJson(spaces). dumps() was never meant to be in the interface")
fun dumps(arg: Any): String {
    return Dumper(arg).dumps()
}
fun Any.objectToJson(spaces:Int=0) = Dumper(this,spaces).dumps()


