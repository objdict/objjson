/**
 * Created by ian on 19/05/2017.
 */
package nz.salect.objjson

import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail
import kotlin.test.Test

/**
 * Created by ian on 18/05/2017.
 */


class CommonTestFromJson {


    // Test fixture for testJsonLoads_case1 - testJsonLoads_case2 below
    private fun assertNestedList_testData2(res: Map<*, *>) {
        // Check if we have the correct top level
        assertEquals(4, res.size)
        assertEquals(setOf("fmform", "subtitle", "title", "bp"), res.keys)

        // Check if we get the fmActionButtons part correct
        val fmform = res["fmform"] as Map<*, *>
        val fmActionButtons = fmform["fmActionButtons"] as List<*>
        assertEquals(2, fmActionButtons.size)
        assertEquals(listOf("a", "b"), fmActionButtons[0])
        assertEquals(listOf(1, 2), fmActionButtons[1])

        // Check other parts
        assertEquals("Member Cards", res["subtitle"])
        assertEquals("Salect", res["title"])

        val bp = res["bp"] as Map<*, *>
        assertEquals(bp.size, 1)
        assertTrue(bp.containsKey("staticPrefix"))
        assertEquals("/html/images/", bp["staticPrefix"])
    }

    // The following tests are looking at the structure of various different formats of JSON strings
    @Test
    fun testJsonLoads_case1() {
        val testData2 = """
        {
            "fmform":{"fmActionButtons": [ ["a","b"],[1,2] ]},
            "subtitle": "Member Cards",
            "title": "Salect",
            "bp":{
               "staticPrefix": "/html/images/"
            }
        }
        """
        val res = testData2.instanceFromJson()
        assertNestedList_testData2(res as Map<*, *>)
    }

    @Test
    fun testJsonLoads_case2() {
        /* This is to cover the case where the nested list could be correctly parsed when it closes after "}"
        * as opposed to the previous test which covers the end-with-comma case e.g.
        * "fmform":{"fmActionButtons": [ ["a","b"],[1,2] ]}  vs
        * "fmform":{"fmActionButtons": [ ["a","b"],[1,2] ]}, */
        val testData3 = """
        {
            "subtitle": "Member Cards",
            "title": "Salect",
            "bp":{
                "staticPrefix": "/html/images/"
            },
            "fmform":{"fmActionButtons": [ ["a","b"],[1,2] ]}
        }
        """
        val res = testData3.instanceFromJson()
        assertNestedList_testData2(res as Map<*, *>)
    }

    // Test fixture for testJsonLoads_listOfDict_case1 - testJsonLoads_listOfDict_case7 below
    private fun assertListOfDict(res: Map<*, *>) {
        // Check if we have the correct top level
        assertEquals(2, res.size)
        assertEquals(setOf("title", "view"), res.keys)

        // Check if we get the view part correct
        val view = res["view"] as List<*>
        assertEquals(3, view.size)

        val viewItem1 = view[0] as Map<*, *>
        assertEquals(2, viewItem1.size)
        assertEquals("a b", viewItem1["A"])
        assertEquals(1, viewItem1["v"])

        val viewItem2 = view[1] as Map<*, *>
        assertEquals(2, viewItem2.size)
        assertEquals(" b", viewItem2["B"])
        assertEquals(2, viewItem2["v"])


        val viewItem3 = view[2] as Map<*, *>
        assertEquals(2, viewItem3.size)
        assertEquals("c", viewItem3["C"])
        assertEquals(3, viewItem3["v"])

        // Check other parts
        assertEquals("test", res["title"])
    }

    // The following tests are looking at the structure of various different formats of JSON strings
    @Test
    fun testJsonLoads_listOfDict_case1() {
        val testData4 = """
        {
            "title": "test",
            "view": [
                { "A": "a b", "v": 1 },
                { "B": " b", "v": 2 },
                { "C": "c", "v": 3 }
            ]
        }
        """
        val res = testData4.instanceFromJson()
        assertListOfDict(res as Map<*, *>)
    }

    @Test
    fun testJsonLoads_listOfDict_case2() {
        val testData4 = """
        {
            "view": [
                { "A": "a b", "v": 1 },
                { "B": " b", "v": 2 },
                { "C": "c", "v": 3 }
            ],
            "title": "test"
        }
        """
        val res = testData4.instanceFromJson()
        assertListOfDict(res as Map<*, *>)
    }

    @Test
    fun testJsonLoads_listOfDict_case3() {
        val testData4 = """
        {
            "view": [
                { "A": "a b", "v": 1 },
                { "B": " b", "v": 2 },
                { "C": "c", "v": 3 }],
            "title": "test"
        }
        """
        val res = testData4.instanceFromJson()
        assertListOfDict(res as Map<*, *>)
    }

    @Test
    fun testJsonLoads_listOfDict_case4() {
        val testData4 = """
        {
            "title": "test",
            "view": [
                { "A": "a b", "v": 1 },
                { "B": " b", "v": 2 },
                { "C": "c", "v": 3 }]
        }
        """
        val res = testData4.instanceFromJson()
        assertListOfDict(res as Map<*, *>)
    }

    @Test
    fun testJsonLoads_listOfDict_case5() {
        val testData4 = """
        {
            "title": "test",
            "view": [
                {
                    "A": "a b",
                    "v": 1
                },
                {
                    "B": " b",
                    "v": 2
                },
                {
                    "C": "c",
                    "v": 3
                }
            ]
        }
        """
        val res = testData4.instanceFromJson()
        assertListOfDict(res as Map<*, *>)
    }

    @Test
    fun testJsonLoads_listOfDict_case6() {
        val testData4 = """
        {
            "view": [
                {
                    "A": "a b",
                    "v": 1
                },
                {
                    "B": " b",
                    "v": 2
                },
                {
                    "C": "c",
                    "v": 3
                }
            ],
            "title": "test"
        }
        """
        val res = testData4.instanceFromJson()
        assertListOfDict(res as Map<*, *>)
    }

    @Test
    fun testJsonLoads_listOfDict_case7() {
        val testData4 = """
        {
            "view": [{ "A": "a b", "v": 1 },
                { "B": " b", "v": 2 },
                { "C": "c", "v": 3 }],
            "title": "test"
        }
        """
        val res = testData4.instanceFromJson()
        assertListOfDict(res as Map<*, *>)
    }

    @Test
    fun testJsonLoads_dictInDict() {
        val testData4 = """
        {
            "fields": {
                "postID": "testField",
                "fmt": {
                    "max": 16,
                    "postPrefix": "field"
                }
            },
            "title": "test"
        }
        """
        val res = testData4.instanceFromJson() as Map<*, *>

        // Check if we have the correct top level
        assertMap(res, setOf("title", "fields"))

        // Check if we get the fields part correct
        assertMap(res["fields"], setOf("postID", "fmt"))

        val view = res["fields"] as Map<*, *>
        assertMap(
            possibleMap = view["fmt"],
            expectedValues = mapOf("max" to 16, "postPrefix" to "field")
        )
    }

    @Test
    fun testJsonLoads_LoadMap_After_NestedList() {
        val testData = """
        {
            "fmform": {
                "fmActionButtons": [
                    ["Update", "Save", "1"]
                ]
            },
            "bp": {
                "title": "Cards",
                "localStyles": ""
            }
        }
        """
        val res = testData.instanceFromJson() as Map<*, *>

        // Check if we have the correct top level
        assertMap(res, setOf("fmform", "bp"))

        // Check if we get the bp part correct
        val bp = res["bp"] as Map<*, *>
        assertMap(
            possibleMap = bp,
            expectedValues = mapOf("title" to "Cards", "localStyles" to "")
        )
    }

    @Test
    fun testJsonLoads_LoadList_After_NestedList() {
        val testData = """
        {
            "fmform": {
                "fmActionButtons": [
                    ["Update", "Save", "1"]
                ]
            },
            "bp": [1,2,3]
        }
        """
        val res = testData.instanceFromJson()
        assertMap(res, setOf("fmform", "bp"))
        assertList((res as Map<*, *>)["bp"], listOf(1, 2, 3))
    }

    /* TODO: IS this "realData" actually what we want? Has it been changed recently? Is this the spec? */
    @Test
    fun testJsonLoads_realData_case1() {
        val testData = """
    {"fmform": {"recLinks": ["ian3"], "fmActionButtons": [["Update", "Save", ""]]}, "view": {"viewName": "MemberView", "idx": 0, "data": [{"id": "_ObjectId:57038330fead63072498287d", "sqlid": 8, "priorRequests": ["make it quick", "no2"], "priorPickUps": ["a salts order"], "cards": [{"key": {"sqlid": 98}, "flags": "C", "label": "card2"}], "stores": [{"key": {"sqlid": 3}, "status": "C"}, {"key": {"sqlid": 4}, "status": "C"}], "locations": [{"key": {"sqlid": 1}, "status": ""}, {"key": {"sqlid": 3}, "status": ""}]}]}}
        """
        val res = testData.instanceFromJson() as Map<*, *>

        // Check if we have the correct top level
        assertMap(res, setOf("fmform", "view"))

        // Check the fmform part
        val fmform = res["fmform"] as Map<*, *>

        // sub-check the `recLinks` part
        val recLinks = fmform["recLinks"]
        assertList(recLinks, listOf("ian3"))

        // sub-check the fmActionButtons part
        assertList(
            fmform["fmActionButtons"],
            listOf(listOf("Update", "Save", ""))
        )

        // Check the view part
        val view = res["view"] as Map<*, *>
        assertMap(view, 3)

        // sub-check the viewName and idx
        assertEquals("MemberView", view["viewName"])
        assertEquals(0, view["idx"])

        // sub-check the data part
        assertList(view["data"], 1)
        assertMap((view["data"] as List<*>)[0], 7)
    }

    @Test
    fun testJsonLoads_realData_case2() {
        val testData = """
     {"view": {"data": [{"priorRequests": ["make it quick", "no2"], "priorPickUps": ["a salts order"], "cards": [{"key": {"sqlid": 0}, "flags": "A", "label": "card2"}], "stores": [{"key": {"sqlid": 1}, "status": "B"}, {"key": {"sqlid": 2}, "status": "C"}], "locations": [{"key": {"sqlid": 3}, "status": "D"}, {"key": {"sqlid": 4}, "status": "E"}]}]}}
        """
        val res = testData.instanceFromJson() as Map<*, *>

        // Check if we have the correct top level
        assertEquals(1, res.size)
        assertEquals(setOf("view"), res.keys)

        // check the `data` in `view`
        val view = res["view"] as Map<*, *>
        val dataItem = (view["data"] as List<*>)[0] as Map<*, *>

        assertMap(
            possibleMap = dataItem,
            expectedKeys = setOf("priorRequests", "priorPickUps", "cards", "stores", "locations")
        )
        assertList(dataItem["priorRequests"], listOf("make it quick", "no2"))
        assertList(dataItem["priorPickUps"], listOf("a salts order"))

        assertList(dataItem["cards"], 1)
        assertMap(
            possibleMap = (dataItem["cards"] as List<*>)[0],
            expectedValues = mapOf(
                "key" to mapOf("sqlid" to 0),
                "flags" to "A",
                "label" to "card2"
            )
        )

        assertList(dataItem["stores"], 2)
        assertMap(
            possibleMap = (dataItem["stores"] as List<*>)[0],
            expectedValues = mapOf("key" to mapOf("sqlid" to 1), "status" to "B")
        )

        assertList(dataItem["locations"], 2)
        assertMap(
            possibleMap = (dataItem["locations"] as List<*>)[0],
            expectedValues = mapOf("key" to mapOf("sqlid" to 3), "status" to "D")
        )
    }

    @Test
    fun testJsonLoads_realData_case3() {
        val testData = """
     {"locations": {"idx": 0, "cases": {}, "hint": "", "label": "locations", "misc": {}, "name": "locations"}}
        """
        val res = testData.instanceFromJson() as Map<*, *>
        assertMap(res, 1)
        assertMap(
            possibleMap = res["locations"],
            expectedValues = mapOf(
                "idx" to 0,
                "cases" to mapOf<Any, Any>(),
                "hint" to "",
                "label" to "locations",
                "misc" to mapOf<Any, Any>(),
                "name" to "locations"
            )
        )
    }

    @Test
    fun testJsonLoads_realData_case4() {
        val testData = """
     {"fmt": {"values": ["e", "c"], "names": ["European", "Chinese"]}, "misc": {}, "postPrefix": "field"}
        """
        val res = testData.instanceFromJson()
        assertMap(res, setOf("fmt", "misc", "postPrefix"))
        assertMap(
            possibleMap = (res as Map<*, *>)["fmt"],
            expectedValues = mapOf(
                "values" to listOf("e", "c"),
                "names" to listOf("European", "Chinese")
            )
        )
    }

    @Test
    fun testJsonLoads_realData_case5() {
        val testData = """
     {"plan": {"items": [{"None": 0}, {"Budget": 1}, {"Business": 2}, {"Trial": 3}]}, "dob":"name"}
        """
        val res = testData.instanceFromJson()
        assertMap(res, setOf("plan", "dob"), mapOf("dob" to "name"))
        assertMap((res as Map<*, *>)["plan"], 1)

        val items = ((res["plan"]) as Map<*, *>)["items"]
        assertList(
            items,
            listOf(
                mapOf("None" to 0),
                mapOf("Budget" to 1),
                mapOf("Business" to 2),
                mapOf("Trial" to 3)
            )
        )
    }

    @Test
    fun testJsonLoads_realData_case6() {
        val testData = """
        {
            "data": [
                {
                    "priorRequests":[
                        "make it quick",
                        "no2"
                    ],
                    "priorPickUps":[
                        "a salts order"
                    ]
                }
            ]
        }
        """
        val res = testData.instanceFromJson()
        assertMap(res, setOf("data"))

        val dataItem = ((res as Map<*, *>)["data"] as List<*>)[0]
        assertMap(
            dataItem,
            mapOf(
                "priorRequests" to listOf("make it quick", "no2"),
                "priorPickUps" to listOf("a salts order")
            )
        )
    }

    @Test
    fun testJsonData_add_space_when_nested_dict_end() {
        val testData = """
     {"plan": {"a": [{"A": 1}, {"B": 2} ]}, "b":"name"}
        """
        val res = testData.instanceFromJson()
        assertMap(res, setOf("plan", "b"), mapOf("b" to "name"))
        assertMap((res as Map<*, *>)["plan"], 1)

        val items = ((res["plan"]) as Map<*, *>)["a"]
        assertList(items, listOf(mapOf("A" to 1), mapOf("B" to 2)))
    }

    @Test
    fun testJsonData_escape() {
        val testData = """{"plan": "a\"b"}"""
        val res = testData.instanceFromJson()
        assertMap(res, mapOf("plan" to "a\"b"))
    }

    @Test
    fun testJsonData_case7() {
        val testData = """
            {"links": [{"entries": [], "label": "Profiles", "pageName": "Profiles"}]}
            """
        val res = testData.instanceFromJson()
        assertMap(res, setOf("links"))

        val item = (res as Map<*, *>)["links"]
        assertList(item, 1)

        val insideMap = (item as List<*>)[0] as Map<*, *>
        assertMap(insideMap, setOf("entries", "label", "pageName"))

        val entries = insideMap["entries"]
        assertList(entries, 0) // Because objjson parse it so

        assertEquals(insideMap["label"], "Profiles")
        assertEquals(insideMap["pageName"], "Profiles")
    }

    @Test
    fun testJsonData_case8() {
        val testData = """
{"links": [{"entries": [], "pageName": "Profiles"}, {"entries": ["the 1st one - use at yourperil!", "Testing Store (no opad)", "Cafe L'Expo"], "pageName": "Stores"}], "fmform": {"recLinks": ["ian3"], "fmActionButtons": [["Update", "Save", ""]]}}
"""
        val res = testData.instanceFromJson()
        assertMap(res, setOf("links", "fmform"))

        val links = (res as Map<*, *>)["links"]
        assertList(links, 2)

        if (links is List<*>) {
            val item1 = links[0] as Map<*, *>
            assertEquals(item1["entries"], emptyList<Any>())
            assertEquals(item1["pageName"], "Profiles")

            val item2 = links[1] as Map<*, *>
            assertEquals(
                item2["entries"],
                listOf("the 1st one - use at yourperil!", "Testing Store (no opad)", "Cafe L'Expo")
            )
            assertEquals(item2["pageName"], "Stores")
        }

        val fmform = res["fmform"]
        assertMap(
            fmform,
            mapOf(
                "recLinks" to listOf("ian3"),
                "fmActionButtons" to listOf(listOf("Update", "Save", ""))
            )
        )
    }

    @Test
    fun testJsonData_case9() {
        val testData = """
{"links": [{"en": [], "l1": "a"}, {"en": [], "l2": "b"}, {"en": ["c", "d"], "l3": "e"}, {"en": ["f", "g"], "l4": "h"}], "fmform": "test"}
"""
        val res = testData.instanceFromJson()
        assertMap(res, setOf("links", "fmform"))

        val links = (res as Map<*, *>)["links"]
        assertList(links, 4)

        if (links is List<*>) {
            val item1 = links[0] as Map<*, *>
            assertEquals(item1["en"], emptyList<Any>())
            assertEquals(item1["l1"], "a")

            val item2 = links[1] as Map<*, *>
            assertEquals(item2["en"], emptyList<Any>())
            assertEquals(item2["l2"], "b")

            val item3 = links[2] as Map<*, *>
            assertEquals(item3["en"], listOf("c", "d"))
            assertEquals(item3["l3"], "e")

            val item4 = links[3] as Map<*, *>
            assertEquals(item4["en"], listOf("f", "g"))
            assertEquals(item4["l4"], "h")
        }
        val fmform = res["fmform"]
        assertEquals("test", fmform)
    }

    @Test
    fun testJsonData_case10() {
        val testData = """
           {
                "embedded":[

                ]
           }
        """
        val res = testData.instanceFromJson()
        val emb = (res as Map<*, *>)["embedded"] as List<*>

        assertList(emb, 0)
    }

    @Test
    fun testJsonData_case11() {
        val testData = """
{"side_nav": [{"i1": [{"i2": []}, {"i3": []}]}]}
        """
        val res = testData.instanceFromJson()
        assertMap(res, setOf("side_nav"))

        val listValue = (res as Map<*, *>)["side_nav"]
        assertList(listValue, 1)

        val i1 = (listValue as List<*>)[0]
        assertMap(i1, setOf("i1"))

        val i1Value = (i1 as Map<*, *>)["i1"]
        assertList(i1Value, 2)

        i1Value as List<*>
        val i2 = i1Value[0]
        assertMap(i2, setOf("i2"))
        assertEquals((i2 as Map<*, *>)["i2"], emptyList<Any>())

        val i3 = i1Value[1]
        assertMap(i3, setOf("i3"))
        assertEquals((i3 as Map<*, *>)["i3"], emptyList<Any>())
    }

    @Test
    fun testJsonData_case11_to_ensure() {
        val testData = """
{"side_nav": [{"i1": [{"i2": []}, {"i3": [{"i4": []}, {"i5": []}]}]}]}
        """
        val res = testData.instanceFromJson()
        assertMap(res, setOf("side_nav"))

        val listValue = (res as Map<*, *>)["side_nav"]
        assertList(listValue, 1)

        val i1 = (listValue as List<*>)[0]
        assertMap(i1, setOf("i1"))

        val i1Value = (i1 as Map<*, *>)["i1"]
        assertList(
            i1Value, listOf(
                mapOf("i2" to listOf<Any>()),
                mapOf(
                    "i3" to listOf(
                        mapOf("i4" to listOf()),
                        mapOf("i5" to listOf<Any>())
                    )
                )
            )
        )
    }

    @Test
    fun testJsonData_case12() {
        val testData = """{"x": [{"g": {}, "h": "H"}]}"""
        val res = testData.instanceFromJson()
        assertMap(res, setOf("x"))

        val data = (res as Map<*, *>)["x"]
        assertList(
            data, listOf(
                mapOf(
                    "g" to mapOf<Any, Any>(),
                    "h" to "H"
                )
            )
        )
    }

    @Test
    fun testJsonData_case_empty_dict() {
        val testData = """{"favourites": [{"items": {}}, {"items": {"1": {"sz": ""}}}]}"""
        val res = testData.instanceFromJson()
        // println(res)
        val favList = (res as Map<*, *>)["favourites"] as List<*>
        assertList(
            favList,
            listOf(
                mapOf<String, Map<*, *>>("items" to emptyMap<Any, Any>()),
                mapOf<String, Map<*, *>>(
                    "items" to mapOf(
                        "1" to mapOf("sz" to "")
                    )
                )
            )
        )
    }

    @Test
    fun testJsonData_complex_nestDict() {
        val testData = """
            {"a": [{"b": {"c": {"d": []}}, "e": 2}]}
            """
        val res = testData.instanceFromJson()
        // println(res)
        assertMap(
            res,
            mapOf(
                "a" to listOf(
                    mapOf(
                        "b" to mapOf(
                            "c" to mapOf(
                                "d" to emptyList<Any>()
                            )
                        ),
                        "e" to 2
                    )
                )
            )
        )
    }

    @Test
    fun testJsonData_complex_nestDict1() {
        val testData = """
            {
              "ordItems": [
                {
                  "options": [
                    {
                      "num": [
                        2
                      ]
                    },
                    {
                      "num": [
                        0,
                        2
                      ]
                    }
                  ],
                }
              ]
            }
            """
        val res = testData.instanceFromJson()
        val firstNum =
            (((((res as Map<*, *>)["ordItems"] as List<*>)[0] as Map<*, *>)["options"] as List<*>)[0] as Map<*, *>)["num"] as List<*>

        assertEquals(2, firstNum[0])
    }

    @Test
    fun loadingListOfListsWithEmptyList() {
        val listData = """{"listyList":[[]]}"""
        val listyList = (listData.instanceFromJson() as Map<*, *>)["listyList"] as List<*>
        val expectedList = listOf<List<Any>>(emptyList())
        assertEquals(expectedList, listyList)
    }

    @Test
    fun loadSimleValue(){
        val data = """{ value: 3 }""".instanceFromJson(false)
        println(" simpleLoad $data")
        assertEquals("3",(data as Map<String,Any>)["value"] as String ?: 11, "simpletest")
    }

}
