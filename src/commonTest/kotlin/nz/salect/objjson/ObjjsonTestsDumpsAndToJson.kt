/**
 * Created by ian on 19/05/2017.
 */
package nz.salect.objjson

import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail
import kotlin.test.Test


/**
 * Created by ian on 18/05/2017.
 */

//fun assertList(listToAssert: Any?, expectedSize: Int) {
//    assertTrue(listToAssert is List<*>)
//    assertEquals(expectedSize, (listToAssert).size, "Size of list:$listToAssert")
//
//}
//fun assertList(listToAssert: Any?,  expectedItems: List<*>) {
//    assertList(listToAssert, expectedItems.size)
//    assertEquals(expectedItems, listToAssert as List<*>, "Items in list")
//}
//
//fun assertMap(possibleMap: Any?, expectedSize: Int) {
//    assertTrue(possibleMap is Map<*, *>)
//    val mapToCheck:Map<*,*> = possibleMap //as Map<*, *>
//    assertEquals(expectedSize, mapToCheck.size, "Size of keys in map:$possibleMap")
//
//}
//fun assertMap(possibleMap: Any?, expectedKeys: Set<*>, includedValues: Map<*, *>? = null) {
//    // if map is all values, then no need to specify keys... dont use this version
//    assertMap(possibleMap, expectedKeys.size)
//    val mapToCheck:Map<*,*> = possibleMap as Map<*, *>
//
//    assertEquals(expectedKeys, mapToCheck.keys, "Assert the keys in map")
//
//
//}
//fun assertMap(possibleMap: Any?, expectedValues: Map<*, *>) {
//    assertMap(possibleMap, expectedValues.keys) //,expectedValues)
//}
//
//const val testData = """
//{ abc:  "4\"a\\b", "d[e]f":5,lst:[1,2,3], eg3:{ "ght":"null", mmn:null, bool: true },lst2:[1,2,3]}"""

//const val testData2 = """
//    {
//        "fmform":{"fmActionButtons": [ ["a","b"],[1,2] ]},
//        "subtitle": "Member Cards",
//        "title": "Salect",
//        "bp":{
//            "staticPrefix": "/html/images/"
//        }
//    }
//"""


class CommonTestDumps {


    @Test
    fun testDumps() {
        //print("testData: ")
        //println(testData)
        val testMap = testData.instanceFromJson()!!
        //println("res: ")
        //println("data is: $testMap, Index is: $testIndex")
        val dump = Dumper(testMap).dumps()
        //println("dump: ")
        //println(dump)
        assertTrue(dump.contains(""""lst": [1"""), "testDumps(): dump contains test string")
        val dump2 = Dumper(loads(dump)).dumps()
        assertEquals(dump, dump2, "testDumps(): reading and dumping matches")
    }

    @Test
    fun testDumpsWithNewLine() {
        //print("testData: ")
        //println(testData)
        val testMap = mapOf(
            "abc" to """hello
            there""".trimMargin()
        )

        val dump = Dumper(testMap).dumps()
        assertTrue(dump.contains("""hello\n"""), "handling new lines")

    }

    @Test
    fun testDumps_case1() {
        val testData = mapOf(
            "name" to "albert",
            "age" to 26,
            "work" to listOf<Any>()
        )

        val res = Dumper(testData).dumps()
        assertEquals("{\"name\": \"albert\", \"age\": 26, \"work\": []}", res)
    }

    @Test
    fun testDumps_case2_boolean() {
        val testData = mapOf<String, Any>(
            "isTrue" to true,
            "isFalse" to false
        )

        val res = Dumper(testData).dumps()
        assertEquals("{\"isTrue\": true, \"isFalse\": false}", res)
    }

    @Test
    fun testDumps_case3_Map() {
        val testData = mapOf<String, Map<*, *>>(
            "products" to emptyMap<Any, Any>()
        )

        val res = Dumper(testData).dumps()
        assertEquals("{\"products\": {}}", res)
    }

    @Test
    fun testEmptyList_should_be_empty_list() {
        val data = "{ \"a\": [ ] } }"
        val res = data.instanceFromJson()
        assertEquals(0, ((res as Map<*, *>)["a"] as List<*>).size)
    }

    @Test
    fun testDump_empty_list() {
        val data = mapOf("a" to emptyList<Any>())
        val res = Dumper(data).dumps()
        assertEquals("{\"a\": []}", res)
    }

    @Test
    fun testDumps_escapes_double_quotes_inside_strings() {
        val quotes = mapOf("a" to """some "string" with "double" quotes""")
        val output = dumps(quotes)
        val expected = """{"a": "some \"string\" with \"double\" quotes"}"""
        assertEquals(expected, output)
    }
    @Test
    fun testMapOfMap(){
        val data = mapOf( "age" to 5,
            "name" to mapOf("first" to "fred","last" to "smith"))
        val expect = """{"age": 5,
            |  "name": {"first": "fred",
            |      "last": "smith"
            |    }
            |}""".trimMargin()
        val output = Dumper(data,4).dumps()
        assertEquals(expect,output,"nested map")
    }
}
