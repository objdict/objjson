/**
 * Created by ian on 19/05/2017.
 */
package nz.salect.objjson

import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail
import kotlin.test.Test

/**
 * Created by ian on 18/05/2017.
 */

fun assertList(listToAssert: Any?, expectedSize: Int) {
    assertTrue(listToAssert is List<*>)
    assertEquals(expectedSize, (listToAssert).size, "Size of list:$listToAssert")

}

fun assertList(listToAssert: Any?, expectedItems: List<*>) {
    assertList(listToAssert, expectedItems.size)
    assertEquals(expectedItems, listToAssert as List<*>, "Items in list")
}

fun assertMap(possibleMap: Any?, expectedSize: Int) {
    assertTrue(possibleMap is Map<*, *>)
    val mapToCheck: Map<*, *> = possibleMap //as Map<*, *>
    assertEquals(expectedSize, mapToCheck.size, "Size of keys in map:$possibleMap")

}

fun assertMap(possibleMap: Any?, expectedKeys: Set<*>, includedValues: Map<*, *>? = null) {
    // if map is all values, then no need to specify keys... dont use this version
    assertMap(possibleMap, expectedKeys.size)
    val mapToCheck: Map<*, *> = possibleMap as Map<*, *>

    assertEquals(expectedKeys, mapToCheck.keys, "Assert the keys in map")

//    expectedValues?.let {
//        expectedValues.keys.forEach { key ->
//            assertEquals(expectedValues[key], mapToCheck[key], "Value of map[$key]")
//        }
//    }
}

fun assertMap(possibleMap: Any?, expectedValues: Map<*, *>) {
    assertMap(possibleMap, expectedValues.keys) //,expectedValues)
}

const val testData = """
{ abc:  "4\"a\\b", "d[e]f":5,lst:[1,2,3], eg3:{ "ght":"null", mmn:null, bool: true },lst2:[1,2,3]}"""

//const val testData2 = """
//    {
//        "fmform":{"fmActionButtons": [ ["a","b"],[1,2] ]},
//        "subtitle": "Member Cards",
//        "title": "Salect",
//        "bp":{
//            "staticPrefix": "/html/images/"
//        }
//    }
//"""


class CommonTest {
    @Test
    fun testMarks() {
        val specials = listOf("\"", "{", "]", ",", "[")
        val testStr = "{ abc, def, { ght, mmn }}"
        var markCount = 0
        val markSequence = "{,,{,"
        val markIndex = listOf(0, 5, 10, 12, 17)
        for (p in iterMarks(testStr, specials, 0)) {
            assertEquals(markIndex[markCount], p?.index, "checking mark pos")
            assertEquals(markSequence[markCount].toString(), p?.mark, "checking mark")
            markCount++
            //println(p)
        }
        assertEquals(5, markCount, "checking found all marks")
    }

    @Test
    fun testParse() {
        val (reshold, index) = Parser(testData,true).parse()
        println(reshold)

        when (reshold) {
            is Map<*, *> -> {
                //println("keys ${reshold.keys}")
                assertTrue("abc" in reshold)
                assertEquals(reshold["abc"], "4\"a\\b")
                assertTrue("lst" in reshold)
                val ls2 = reshold["lst2"]
                when (ls2) {
                    is List<*> -> assertEquals(ls2[1], 2)
                    else -> assertTrue(false, "ls2 should be a list")
                }
                assertTrue("eg3" in reshold)
                val eg3 = reshold["eg3"]
                when (eg3) {
                    is Map<*, *> -> {
                        assertEquals("null", eg3["ght"])
                        assertEquals(null, eg3["mmn"])
                        assertEquals(true, eg3["bool"])
                    }
                    else -> fail("eg3 wrong type")
                }

            }
            else -> assertTrue(false, "parse should have returned a map")
        }

        assertTrue(index > 24)
    }

    @Test
    fun testParseControlCharacters() {
        // Some of these characters are supported in Kotlin in IDE so need to use the unicode value when not supported
        // See asserts below on "when clause"!

        // Note that the \v and \0 escapes are not allowed in JSON strings.

        // '\u0000' or '\0' -- null
        // '\u0007' or '\a' -- bell
        // '\u0008' or '\b' -- backspace
        // '\u0009' or '\t' -- tab
        // '\u000A' or '\n' -- line feed
        // '\u000C' or '\f' -- form feed
        // '\u000D' or '\r' -- carriage return

        // Not supported
        // '\u000B' or '\v' -- vertical tab

        val controlCharTestData = """{
            |nullChar: "\0",
            |bellChar: "\a",
            |backspaceChar: "\b",
            |tabChar: "\t",
            |lineFeedChar: "hello \n there!",
            |formFeedChar: "\f",
            |carriageReturnChar: "\r",
            |multipleChars: "\0\b\n\r",
            |verticalTab: "\v"}
            |""".trimMargin()
        val (resHold, _) = Parser(controlCharTestData,true).parse()
        when (resHold) {
            is Map<*, *> -> {
                assertEquals(resHold["nullChar"], "\u0000", "Null character assert fail")
                assertEquals(resHold["bellChar"], "\u0007", "Bell character assert fail")
                assertEquals(resHold["backspaceChar"], "\b", "Backspace character assert fail")
                assertEquals(resHold["tabChar"], "\t", "Tab character assert fail")
                assertEquals(resHold["lineFeedChar"], "hello \n there!", "Linefeed character assert fail")
                assertEquals(resHold["formFeedChar"], "\u000C", "Form feed character assert fail")
                assertEquals(resHold["carriageReturnChar"], "\r", "Carriage return character assert fail")
                assertEquals(resHold["multipleChars"], "\u0000\b\n\r", "Multiple control character assert fail")

                // Vertical tab is not supported so should return "v" i.e. the stripped control code
                assertEquals(resHold["verticalTab"], "v", "Vertical tab character assert fail")
            }
            else -> assertTrue(false, "Parsing 'controlCharTestData' should return a map to resHold")
        }
    }

    @Test
    fun testLoads() {
        // loads calls Parser and returns the data as above anyway
        val res = "{\"abc\":2}".instanceFromJson() as Map<*, *>
        assertEquals(2, res["abc"])
    }

    @Test
    fun testLoadsList() {
        // loads calls Parser and returns the data as above anyway
        val res = loads("[ 1,2,3]")
        when (res) {
            is List<*>
            -> assertEquals(2, res[0])
            else -> println("does not parse outer level list")
        }
    }


}
object AdvancedFeaturesSpec: Spek({

    describe("Using 'fromJsonNameMap' to allow alternate Names") {
        //val testName by memoized { testString }

        describe("companion with 'fromJsonNameMap'") {
            val dataWithMemo  by memoized{
                // is for when data used by several tests
                // so this one is just a sample
                """{ oldname: 4} """.instanceFromJson()
            }

            it("allows use of oldname in json") {
                val testName = """{ oldname: 4} """.instanceFromJson()
                testName?.also {
                    assertEquals(4, it["oldname"],"wrong value using nameMap")
                } ?: throw Exception("Did not parse")
            }
            it("chooses new name value when both old and new are present") {
                val testName = """{ newName: 7, oldname: 4} """.instanceFromJson()
                testName?.also {
                    assertEquals(7 , it["newName"],"did not find newname with nameMap")
                } ?: throw Exception("Did not parse")
            }
        }
    }
})
