/**
 * Created by ian on 19/05/2017.
 */
package nz.salect.objjson


import java.math.BigDecimal
import java.math.BigInteger
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.*

actual typealias PointNum = BigDecimal
actual fun String.toPointNum() = this.toBigDecimalOrNull()

val <T:Any> List<T>.nullForEmpty:List<T>? get() = if(this.isEmpty()) null else this

fun String.containsAny(data:List<String>):Boolean{
    data.forEach{
        if(this.contains(it)) return true
    }
    return false
}

typealias ClassMap = Map<String, KClass<Any>>

fun KParameter.typeString() = this.type.classifier.toString()
fun KParameter.isKotlinMap() = this.typeString().contains("kotlin.collections.Map")
fun KParameter.isKotlinList() = this.typeString().contains("kotlin.collections.List")
fun KParameter.isKotlinType() = this.typeString().run{
    (this.startsWith("class kotlin.")||this.containsAny(listOf("java.math.BigDecimal")))
        &&!this.contains("collections")

}
fun String.isCustomClass() = !this.startsWith("class kotlin")
fun KParameter.isCustomClass() = this.typeString().isCustomClass()

fun valFromString(typeString: String,inval: Any?):Any? {
    when(val sub =typeString.substringAfter(".")) {
        "String" -> return inval
        "Int" -> return (inval as String).toIntOrNull()
        "Long" -> return (inval as String).toLongOrNull()
        "Byte" -> return (inval as String).toByteOrNull()
        "Short" -> return (inval as String).toShortOrNull()
        //"Decimal" -> return (inval as String).toDecimalOrNull()
        "math.BigDecimal" -> return (inval as String).toBigDecimalOrNull()
        else -> return inval
    }
}
fun listFromString(typeString: String,invals: List<Any?>):List<Any?> {
    // should be able to replace with
    // return invals.map{valFromString(typeString,it)}

    when(val sub =typeString.substringAfter(".")) {
        "String" -> return invals
        "Int" -> return invals.map{(it as String).toIntOrNull()}
//        "Long" -> return (inval as String).toLongOrNull()
//        "Byte" -> return (inval as String).toByteOrNull()
//        "Short" -> return (inval as String).toShortOrNull()
//        //"Decimal" -> return (inval as String).toDecimalOrNull()
//        "BigDecimal" -> return (inval as String).toBigDecimalOrNull()
        else -> return invals
    }
}

fun processList(param: KParameter, value: Any?, other: Map<String, List<KClass<*>>>?): Any? {
    val itemClass = param.type.arguments[0].type?.classifier as KClass<*>
    val itemTypeName = itemClass.toString()

    //result = itemTypeName.isCustomClass()

    if(itemTypeName.isCustomClass()) {
        val result = mutableListOf<Any>()
        (value as List<*>).forEach {
            val temp = (it as Map<String, *>).instanceFromMap(itemClass, other = other)
            temp?.let {
                result.add(temp)
            }
        }
        if (result.isNotEmpty()) return result
    }else{
        return listFromString(itemTypeName,value as List<Any?>)
    }
    return value
}


actual fun <T : Any> Map<String, Any?>.instanceFromMap(
    cls: KClass<T>,
    raises:Boolean,
    other: Map<String,List<KClass<*>>>? //map of alt classes for constructor
): T? {
    fun generateValue(param: KParameter, paramName:String, otherClasses: Map<String, List<KClass<*>>>?): Any? {

        fun KParameter.isListOfObjects(): Boolean {
            var result = false
            if (this.typeString().contains("kotlin.collections.List")) {
                val itemTypeName = this.type.arguments[0].type?.classifier.toString()
                result = itemTypeName.isCustomClass() // not just list..list of custom
            }
            return result
        }


        fun String.isNullableClass() = this.endsWith("?")

        var value = this[paramName]
        val paramTypeString = param.typeString()

        when{
            param.isKotlinType() -> {
                value = valFromString(paramTypeString, value)
            }

            param.isKotlinMap()->{

                val valueClassType = param.type.arguments[1].type?.classifier as KClass<*>
                val isValueParamCustomClass = valueClassType.toString().isCustomClass()
                val result = mutableMapOf<String, Any?>()
                when (value) {
                    is Map<*, *> -> {
                        value.forEach {
                            //key, value_ ->
                            var newValue: Any? = it.value

                            if (isValueParamCustomClass)
                                newValue = (it.value as Map<String, *>)
                                    .instanceFromMap(valueClassType, raises = raises, other = otherClasses)

                            result[it.key.toString()] = newValue
                        }
                    }
                    else -> null
                }
                if (result.isNotEmpty()) value = result
            }
            param.isKotlinList() -> value = processList(param, value, otherClasses)

            param.isCustomClass() -> {
                when {
                    value is Map<*, *> -> {
                        value = (value as Map<String, *>).instanceFromMap(
                            param.type.classifier as KClass<*>,
                            raises = raises,
                            other = otherClasses
                        )
                    }
                    //paramTypeString.isNullableClass() && ... test for nullable class fails -so try to use the null
                    value == null -> {
                    } // will return the null
                    else ->
                        throw IllegalArgumentException("The value for a class type parameter(${param.type}) must be a Map<String, Any>, the current value is $value")
                }
            }
        }

        return value
    }

//    val klas = cls //:KClass<Any> = T::class as KClass<Any>
//    var funk: KFunction<*>? = null

    val companionFuncs = cls.companionObject?.declaredMemberFunctions
    val factoryFunks = companionFuncs?.filter{it.name == "fromJson"}?.nullForEmpty
    val interfaceFunk = companionFuncs?.filter{it.name == "fromJsonInterfaces"}?.nullForEmpty
    val nameMapFunk = companionFuncs?.filter{it.name == "fromJsonNameMap"}?.nullForEmpty

    var otherClassesForInterface = interfaceFunk?.let {
        try{
            (it.getOrNull(0) as KFunction<Map<String,List<KClass<*>>>>?)?.let{
                val pmap = mapOf( it.parameters[0] to cls.companionObjectInstance)
                it.callBy(pmap)
            }

        }
        catch (e:Exception){
            null
        }

     //   Map<String, List<KClass<*>>>? = null
    }

    val nameMap:Map<String, String> = nameMapFunk?.let{
        try{
            (it.getOrNull(0) as KFunction<Map<String,String>>?)?.let{
                val pmap = mapOf( it.parameters[0] to cls.companionObjectInstance)
                it.callBy(pmap).toList().associate {  (k,v) -> Pair(v,k)}
            }

        }
        catch (e:Exception){
            null
        }
    } ?: mapOf()

    val constructors: List<KFunction<T>> = when {
        (factoryFunks != null) ->
            factoryFunks as List < KFunction < T > >//funk as KFunction<T>

        else -> {
            cls.primaryConstructor?.let {
                listOf(it)
            } ?: other?.get(cls.simpleName)?.let{
                it.map{ it.primaryConstructor as KFunction<T>}
            } ?: throw IllegalArgumentException("no primary constructor ${cls.simpleName}")

        }
    }
    lateinit var ex: Exception
    fun KParameter.mappedName(m: Map<String, Any?>):String {
        if(this.name in m) return this.name.toString()
        return nameMap.getOrDefault(this.name.toString(), this.name.toString())
    }
//    (parm:KParameter):KParameter {
//        return parm //KParameter(index = parm.index, parm.isOptional)
//    }

    constructors.forEach { cons ->
        try {

            var valuesMap = cons.parameters.filter { it.mappedName(this) in this }
                .associateBy(
                    { it },
                    { generateValue(it, it.mappedName(this) , otherClassesForInterface) }
                )
            val valuesMutableMap = valuesMap.toMutableMap() //:Map<Int,Int> = valuesMap
            factoryFunks?.let {
                valuesMutableMap[cons.parameters[0]] = cls.companionObjectInstance
            }

            return cons.callBy(valuesMutableMap) as T
        } catch (e: Exception) {  ex = e  }
    }
    if(raises)
        throw(ex)
    return null
//    }catch(e:IllegalArgumentException){
//        return null
//    }
    }



fun <T : Any> instanceFromMapUsing__Type__(cls: KClass<T>, vals: Map<*, *>): T {
    fun generateValue(param: KParameter): Any? {
        fun isCustomClass(classDef: KClass<*>): Boolean {
            val qualifiedName = classDef.qualifiedName
            val simpleName = classDef.simpleName
            var result = false
            if (qualifiedName == null && simpleName != null) {
                result = true
            } else if (qualifiedName != null) {
                result = !qualifiedName.startsWith("kotlin.")
            }
            return result
        }

        fun getClassDef(
            paramIn: KParameter,
            isList: Boolean = false,
            isMap: Boolean = false
        ): KClass<*> {
            if (isList) return param.type.arguments[0].type?.classifier as KClass<*>
            if (isMap) return param.type.arguments[1].type?.classifier as KClass<*>
            return paramIn.type.classifier as KClass<*>
        }

        fun isListOfObjects(value: Any?, param: KParameter): Boolean {
            var result = false
            if (value is List<*>) {
                val itemClassDef = getClassDef(param, true)
                if (isCustomClass(itemClassDef)) result = true
            }
            return result
        }

        var value = vals[param.name]

        if (value is Map<*, *>) {
            val valueClassDef = getClassDef(param)
            val isCustom = isCustomClass(valueClassDef)
            val result = mutableMapOf<Any?, Any?>()

            if (isCustom) {
                value = instanceFromMapUsing__Type__(valueClassDef, value)
            } else {
                val subValueClassDef = getClassDef(param, false, true)
                val isValueCustom = isCustomClass(subValueClassDef)

                if (isValueCustom) {
                    value.forEach { key, value_ ->
                        result[key] = instanceFromMapUsing__Type__(subValueClassDef, value_ as Map<*, *>)
                    }
                } else {
                    value.forEach { key, value_ ->
                        if (key.toString() == "__type__") return@forEach
                        result[key] = value_
                    }
                }

                if (result.isNotEmpty()) value = result
            }
        } else if (isListOfObjects(value, param)) {
            val itemClass = param.type.arguments[0].type?.classifier as KClass<*>
            val result = mutableListOf<Any>()
            (value as List<*>).forEach {
                val temp = instanceFromMapUsing__Type__(itemClass, it as Map<*, *>)
                result.add(temp)
            }
            if (result.isNotEmpty()) value = result
        } else if (isCustomClass(getClassDef(param))) {
            if (value is Map<*, *>) {
                value = instanceFromMapUsing__Type__(
                    getClassDef(param),
                    value
                )
            } else {
                throw IllegalArgumentException(
                    "The according value for a class type parameter(${param.type}) must be a Map<String, Any>, the current value is $value"
                )
            }
        }

        return value
    }

    val cons = cls.primaryConstructor!!
    val valmap = cons.parameters.associateBy(
        { it },
        { generateValue(it) }
    )
    val data = cons.callBy(valmap)
    return data
}

@Deprecated("move to Parser(arg,doNums=true)")
fun oloads(arg: String, mp: ClassMap = mapOf()): Any {

    //for( )
    //dostring()
    return Parser(arg,true).parse().first
}

// Renamed to instanceFromJsonWthType (was instanceFromJson)
@Deprecated("move to InstanceFromJson(cls: KClass<T>)")
fun <T : Any> String.instanceFromJsonWithType(cls: KClass<T>): T {
    val res = oloads(this)
    return instanceFromMapUsing__Type__(
        cls, when (res) {
            is Map<*, *> -> res
            else -> mapOf<String, Any>()
        }
    )
}

actual fun classToFields(data:Any,dumper: Dumper):String{
    val tdata = data::class as KClass
    val props = tdata.memberProperties?.map{it.name}

    val primParms = tdata.primaryConstructor?.parameters
    val primes = primParms
        ?.map{ it.name }
        ?.filter{props.contains(it)}
    val nullDefaults = primParms
        ?.filter{it.isOptional}
        ?.map{it.name}
    val (excludeFun, extrasFun) //, nullIncludesFun )
        = listOf("objJsonExcludes","objJsonExtras")
        .map{
                try{
                    data.javaClass.getMethod(it)
                }
                catch(e:Exception){
                    null
                }
            }

    val excludes:List<String> =
        try {   excludeFun?.invoke(data) as List<String>
        }catch(e:Exception) { listOf()
        }

    val extras:Map<String,Any?>? =
//        try {
            extrasFun?.invoke(data) as Map<String,Any?>?
//        }catch(e:Exception) { mapOf()
//        }


    val source = primes ?: props

        val map = source.filter{  !excludes.contains(it)}.associateBy({ it} ,

            {data.javaClass.getMethod("get" + it?.capitalize()).invoke(data)}
        ).filter{(k,v) -> v!= null || !(nullDefaults?.contains(k)?:false) }.toMutableMap() //as Map<String, Any?>

    extras?.forEach{(k,v) -> map[k] = v}


    return Dumper(map,dumper).dumps()
}

fun <T : Any> Map<String, Any>.instanceOfJson(cls: KClass<T>): T {
    // so not confuse with String.instanceFromJson  - uncertain of what this is designed to do?
    return instanceFromMapUsing__Type__(cls, this)
}
