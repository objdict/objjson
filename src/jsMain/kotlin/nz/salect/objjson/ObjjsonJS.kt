package nz.salect.objjson

import kotlin.reflect.KClass
import kotlin.reflect.KParameter

//import kotlin.reflect.full.companionObject
//import kotlin.reflect.full.companionObjectInstance
//import kotlin.reflect.full.declaredMemberFunctions
//import kotlin.reflect.full.primaryConstructor

actual fun <T : Any> Map<String, Any?>.instanceFromMap(
    cls: KClass<T>,
    raises: Boolean,
    other: Map<String, List<KClass<*>>>?
    ): T? {
//    fun generateValue(param: KParameter): Any? {
//
//        fun String.isCustomClass() = !this.startsWith("class kotlin")
//        fun KParameter.typeString() = this.type.classifier.toString()
//
//        fun isListOfObjects(paramName: String): Boolean {
//            var result = false
//            if (paramName.startsWith("kotlin.collections.List")) {
//                val itemTypeName = paramName.substring(24).removeSuffix(">")
//                if (!itemTypeName.startsWith("kotlin")) result = true
//            }
//            return result
//        }
//
//        fun isCustomClass(paramName: String): Boolean {
//            return !paramName.startsWith("kotlin")
//        }
//
//        var value = vals[param.name]
//        val paramName = param.type.toString()
//
//        if (paramName.startsWith("kotlin.collections.Map")) {
//            val valueClassType = param.type.arguments[1].type?.classifier as KClass<*>
//            val isValueParamCustomClass = !valueClassType.toString().startsWith("class kotlin")
//            val result = mutableMapOf<Any?, Any?>()
//            (value as Map<*, *>).forEach { key, value_ ->
//                var newValue: Any? = value_
//
//                if (isValueParamCustomClass)
//                    newValue = instanceFromMap(valueClassType, value_ as Map<*, *>)
//
//                result[key] = newValue
//            }
//            if (result.isNotEmpty()) value = result
//        } else if (isListOfObjects(paramName)) {
//            val itemClass = param.type.arguments[0].type?.classifier as KClass<*>
//            val result = mutableListOf<Any>()
//            (value as List<*>).forEach {
//                val temp = instanceFromMap(itemClass, it as Map<*, *>)
//                result.add(temp)
//            }
//            if (result.isNotEmpty()) value = result
//        } else if (isCustomClass(paramName)) {
//            if (value is Map<*, *>) {
//                value = instanceFromMap(
//                        param.type.classifier as KClass<*>,
//                        value
//                )
//            } else {
//                throw IllegalArgumentException("The according value for a class type parameter(${param.type}) must be a Map<String, Any>, the current value is $value")
//            }
//        }
//
//        return value
//    }
    val klas = cls //:KClass<Any> = T::class as KClass<Any>
    //val cons = klas() //.primaryConstructor!!
//    val valmap = cons.parameters.associateBy(
//            {it},
//            {generateValue(it)}
//    )
//    val data = cons.callBy(valmap) //as T
    throw Exception("Cannot instance from map in kotlinJS")
}
actual fun classToFields(data:Any, dumper: Dumper) = "unknownjs"
actual typealias PointNum = Double
actual fun String.toPointNum() = toDoubleOrNull()
