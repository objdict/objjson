package nz.salect.objjson

val menuRawStr = """
{
"rc": 0,
"rt": "Menu for OrderPOS",
"menu": 

{
"__type__": "MenuObject",
"filters": 

{
"B": 

{
"flags": "",
"foodtype": "Bar Food",
"hours": "",
"itemType": "",
"menuCode": "B",
"menuComment": "",
"menuTitle": "Bar Food",
"onFilters": "",
"store": "1064",
"whereFilter": "",
"workArea": "main",
"__type__": "MenuFilter"
},
"C": 

{
"flags": "",
"foodtype": "Cocktails",
"hours": "",
"itemType": "",
"menuCode": "C",
"menuComment": "",
"menuTitle": "Cocktails",
"onFilters": "",
"store": "1064",
"whereFilter": "",
"workArea": "main",
"__type__": "MenuFilter"
},
"W": 

{
"flags": "",
"foodtype": "Wine",
"hours": "",
"itemType": "",
"menuCode": "W",
"menuComment": "",
"menuTitle": "Wine",
"onFilters": "",
"store": "1064",
"whereFilter": "",
"workArea": "main",
"__type__": "MenuFilter"
},
"L": 

{
"flags": "",
"foodtype": "Beer and Cider",
"hours": "",
"itemType": "",
"menuCode": "L",
"menuComment": "",
"menuTitle": "Beer and Cider",
"onFilters": "",
"store": "1064",
"whereFilter": "",
"workArea": "main",
"__type__": "MenuFilter"
},
"N": 

{
"flags": "",
"foodtype": "Juice|Shakes|Soda",
"hours": "",
"itemType": "",
"menuCode": "N",
"menuComment": "",
"menuTitle": "Juice|Shakes|Soda",
"onFilters": "",
"store": "1064",
"whereFilter": "",
"workArea": "main",
"__type__": "MenuFilter"
},
"T": 

{
"flags": "",
"foodtype": "Coffee and Tea",
"hours": "",
"itemType": "",
"menuCode": "T",
"menuComment": "",
"menuTitle": "Coffee and Tea",
"onFilters": "",
"store": "1064",
"whereFilter": "",
"workArea": "main",
"__type__": "MenuFilter"
}
},
"products": 

{
"Brightwater Nelson": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Brightwater Nelson",
"price": "0.00",
"__type__": "ValObj"
},
"itemCode": "Brightwater Nelson",
"onFilters": 

[
"W"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Brightwater Options",
"max": 1,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Glass",
"price": "10.00",
"__type__": "ProdOptValue"
},

{
"name": "Bottle",
"price": "55.00",
"__type__": "ProdOptValue"
}
],
"label": "Brightwater",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Wine",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Brightwater Nelson Chardonnay",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Brightwater Sophies": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Brightwater Sophies",
"price": "0.00",
"__type__": "ValObj"
},
"itemCode": "Brightwater Sophies",
"onFilters": 

[
"W"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Sophies Options",
"max": 1,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Glass",
"price": "9.00",
"__type__": "ProdOptValue"
},

{
"name": "Bottle",
"price": "45.00",
"__type__": "ProdOptValue"
}
],
"label": "Sophies",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Wine",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Brightwater Sophies Kiss Rose",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Lemon Honey & Ginger": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Lemon Honey & Ginger",
"price": "4.00",
"__type__": "ValObj"
},
"itemCode": "Lemon Honey & Ginger",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Hot/Cold",
"max": 1,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Hot",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Cold",
"price": "0.00",
"__type__": "ProdOptValue"
}
],
"label": "",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Lemon Honey & Ginger",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Grilled Chorizo": 

{
"__type__": "MenuProduct",
"base": {
"name": "Grilled Chorizo ",
"price": "10.00",
"__type__": "ValObj"
},
"itemCode": "Grilled Chorizo",
"onFilters": 

[
"B"
],
"options": 

[],
"dept": null,
"foodtype": "Bar Food",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Grilled Chorizo w/Lemon",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Edamame Beans": 

{
"__type__": "MenuProduct",
"base": {
"name": "Edamame Beans",
"price": "6.60",
"__type__": "ValObj"
},
"itemCode": "Edamame Beans",
"onFilters": 

[
"B"
],
"options": 

[],
"dept": null,
"foodtype": "Bar Food",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Edamame Beans w/Sea Salt",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Mini Potato Hashies": 

{
"__type__": "MenuProduct",
"base": {
"name": "Mini Potato Hashies",
"price": "10.00",
"__type__": "ValObj"
},
"itemCode": "Mini Potato Hashies",
"onFilters": 

[
"B"
],
"options": 

[],
"dept": null,
"foodtype": "Bar Food",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Mini Potato Hashies w/Parmesan Tomato Sauce and Aioli",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Toasted Sandwich": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Toasted Sandwich",
"price": "5.00",
"__type__": "ValObj"
},
"itemCode": "Toasted Sandwich",
"onFilters": 

[
"B"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Pineapple",
"max": 1,
"min": 0,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Pineapple",
"price": "1.00",
"__type__": "ProdOptValue"
}
],
"label": "Pin",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Bar Food",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Toasted Sandwich w/Ham & Cheese",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Affogato w/Liqueur": 

{
"__type__": "MenuProduct",
"base": {
"name": "Affogato w/Liqueur",
"price": "12.50",
"__type__": "ValObj"
},
"itemCode": "Affogato w/Liqueur",
"onFilters": 

[
"B"
],
"options": 

[],
"dept": null,
"foodtype": "Bar Food",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Affogato w/Liqueur",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Whiskey and Apple": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Whiskey and Apple",
"price": "14.00",
"__type__": "ValObj"
},
"itemCode": "Whiskey and Apple",
"onFilters": 

[
"C"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Whiskey Options",
"max": 4,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Whiskey",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Apple",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Lemon",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Bitter",
"price": "0.00",
"__type__": "ProdOptValue"
}
],
"label": "Whiskey",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Cocktails",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Whiskey and Apple",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Classic Negroni/Boulevardier": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Classic Negroni/Boulevardier",
"price": "16.50",
"__type__": "ValObj"
},
"itemCode": "Classic Negroni/Boulevardier",
"onFilters": 

[
"C"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Negroni Options",
"max": 3,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Gin/Bourbon",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Campart",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Sweet Vermouth",
"price": "0.00",
"__type__": "ProdOptValue"
}
],
"label": "Negroni",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Cocktails",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Classic Negroni/Boulevardier",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"No Scotch": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "No Scotch",
"price": "17.00",
"__type__": "ValObj"
},
"itemCode": "No Scotch",
"onFilters": 

[
"C"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "No Scotch Options",
"max": 5,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Whiskey",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Lemon",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Pineapple",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Ginger",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Absinthe",
"price": "0.00",
"__type__": "ProdOptValue"
}
],
"label": "No Scotch",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Cocktails",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "No Scotch",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Flat White Martini": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Flat White Martini",
"price": "15.00",
"__type__": "ValObj"
},
"itemCode": "Flat White Martini",
"onFilters": 

[
"C"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Martini Options",
"max": 4,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Vodka",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Kahlua",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Baileys",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Espresso",
"price": "0.00",
"__type__": "ProdOptValue"
}
],
"label": "Martini",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Cocktails",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Flat White Martini",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Gancia Prosecco": 

{
"__type__": "MenuProduct",
"base": {
"name": "Gancia Prosecco",
"price": "12.50",
"__type__": "ValObj"
},
"itemCode": "Gancia Prosecco",
"onFilters": 

[
"W"
],
"options": 

[],
"dept": null,
"foodtype": "Wine",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Gancia Prosecco Brut NV 200ml Bottle",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"DArenberg": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "DArenberg",
"price": "0.00",
"__type__": "ValObj"
},
"itemCode": "DArenberg",
"onFilters": 

[
"W"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "DArenberg Options",
"max": 1,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Glass",
"price": "10.00",
"__type__": "ProdOptValue"
},

{
"name": "Bottle",
"price": "50.00",
"__type__": "ProdOptValue"
}
],
"label": "DArenberg",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Wine",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "DArenberg Stump Jump GSM",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"House Port/Sherry": 

{
"__type__": "MenuProduct",
"base": {
"name": "House Port/Sherry",
"price": "12.50",
"__type__": "ValObj"
},
"itemCode": "House Port/Sherry",
"onFilters": 

[
"W"
],
"options": 

[],
"dept": null,
"foodtype": "Wine",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "House Port/Cherry 90ml glass",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Craft Beer": 

{
"__type__": "MenuProduct",
"base": {
"name": "Craft Beer",
"price": "9.50",
"__type__": "ValObj"
},
"itemCode": "Craft Beer",
"onFilters": 

[
"L"
],
"options": 

[],
"dept": null,
"foodtype": "Beer and Cider",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Craft Beer 355ml Can",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Peckham Craft": 

{
"__type__": "MenuProduct",
"base": {
"name": "Peckham Craft",
"price": "9.00",
"__type__": "ValObj"
},
"itemCode": "Peckham Craft",
"onFilters": 

[
"L"
],
"options": 

[],
"dept": null,
"foodtype": "Beer and Cider",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Peckham Craft Cider 355ml Can",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Ginger Fusion": 

{
"__type__": "MenuProduct",
"base": {
"name": "Ginger Fusion",
"price": "9.50",
"__type__": "ValObj"
},
"itemCode": "Ginger Fusion",
"onFilters": 

[
"L"
],
"options": 

[],
"dept": null,
"foodtype": "Beer and Cider",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Ginger Fusion Alcoholic Ginger Beer",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Low Alcohol Beer": 

{
"__type__": "MenuProduct",
"base": {
"name": "Low Alcohol Beer",
"price": "8.00",
"__type__": "ValObj"
},
"itemCode": "Low Alcohol Beer",
"onFilters": 

[
"L"
],
"options": 

[],
"dept": null,
"foodtype": "Beer and Cider",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Low Alcohol Beer 2%ABV 355ml",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Kronenbourg": 

{
"__type__": "MenuProduct",
"base": {
"name": "Kronenbourg",
"price": "7.00",
"__type__": "ValObj"
},
"itemCode": "Kronenbourg",
"onFilters": 

[
"L"
],
"options": 

[],
"dept": null,
"foodtype": "Beer and Cider",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Kronenbourg 355ml Bottle",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Fruit Juice": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Fruit Juice",
"price": "4.00",
"__type__": "ValObj"
},
"itemCode": "Fruit Juice",
"onFilters": 

[
"N"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Fruit Juice Options",
"max": 4,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Apple",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Cranberry",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Orange",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Pineapple",
"price": "0.00",
"__type__": "ProdOptValue"
}
],
"label": "Fruit Juice",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Juice|Shakes|Soda",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Fruit Juice",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Six Barrel Co. Soda": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Six Barrel Co. Soda",
"price": "5.00",
"__type__": "ValObj"
},
"itemCode": "Six Barrel Co. Soda",
"onFilters": 

[
"N"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Barrel Options",
"max": 5,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Lemonade",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Lime",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Hibiscus",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Celery Tonic",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Ice-cream float",
"price": "1.50",
"__type__": "ProdOptValue"
}
],
"label": "Barrel",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Juice|Shakes|Soda",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Six Barrel Co. Soda",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Fentimans Soda": 

{
"__type__": "MenuProduct",
"base": {
"name": "Fentimans Soda",
"price": "6.00",
"__type__": "ValObj"
},
"itemCode": "Fentimans Soda",
"onFilters": 

[
"N"
],
"options": 

[],
"dept": null,
"foodtype": "Juice|Shakes|Soda",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Fentimas Soda & Suger Free Option",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Black": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Black",
"price": "3.50",
"__type__": "ValObj"
},
"itemCode": "Black",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Milk Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Milk Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Milk",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Syrup Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Syrup Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Syrup",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Size Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Size",
"__type__": "ValObj"
},
"values": 

[],
"label": "Size",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Black",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Flat White": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Flat White",
"price": "4.00",
"__type__": "ValObj"
},
"itemCode": "Flat White",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Milk Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Milk Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Milk",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Syrup Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Syrup Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Syrup",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Size Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Size",
"__type__": "ValObj"
},
"values": 

[],
"label": "Size",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Flat White",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Cappuccino": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Cappuccino",
"price": "4.00",
"__type__": "ValObj"
},
"itemCode": "Cappuccino",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Milk Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Milk Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Milk",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Syrup Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Syrup Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Syrup",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Size Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Size",
"__type__": "ValObj"
},
"values": 

[],
"label": "Size",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Cappuccino",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Hot Chocolate": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Hot Chocolate",
"price": "4.00",
"__type__": "ValObj"
},
"itemCode": "Hot Chocolate",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Milk Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Milk Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Milk",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Syrup Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Syrup Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Syrup",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Size Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Size",
"__type__": "ValObj"
},
"values": 

[],
"label": "Size",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Hot Chocolate",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Mochaccino": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Mochaccino",
"price": "4.50",
"__type__": "ValObj"
},
"itemCode": "Mochaccino",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Milk Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Milk Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Milk",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Syrup Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Syrup Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Syrup",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Size Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Size",
"__type__": "ValObj"
},
"values": 

[],
"label": "Size",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Mochaccino",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Latte": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Latte",
"price": "4.50",
"__type__": "ValObj"
},
"itemCode": "Latte",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Milk Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Milk Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Milk",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Syrup Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Syrup Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Syrup",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Size Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Size",
"__type__": "ValObj"
},
"values": 

[],
"label": "Size",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Latte",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Chai Latte": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Chai Latte",
"price": "5.00",
"__type__": "ValObj"
},
"itemCode": "Chai Latte",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Milk Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Milk Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Milk",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Syrup Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Syrup Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Syrup",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Size Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Size",
"__type__": "ValObj"
},
"values": 

[],
"label": "Size",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Chai Latte",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Turmeric Latte": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Turmeric Latte",
"price": "5.00",
"__type__": "ValObj"
},
"itemCode": "Turmeric Latte",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Milk Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Milk Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Milk",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Syrup Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Syrup Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Syrup",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Size Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Size",
"__type__": "ValObj"
},
"values": 

[],
"label": "Size",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Turmeric Latte",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Iced Choc Coffee": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Iced Choc Coffee",
"price": "5.00",
"__type__": "ValObj"
},
"itemCode": "Iced Choc Coffee",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Milk Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Milk Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Milk",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Syrup Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Syrup Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Syrup",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Size Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Size",
"__type__": "ValObj"
},
"values": 

[],
"label": "Size",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Iced Choc Coffee",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Tea": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Tea",
"price": "4.00",
"__type__": "ValObj"
},
"itemCode": "Tea",
"onFilters": 

[
"T"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Milk Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Milk Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Milk",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Syrup Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Syrup Options",
"__type__": "ValObj"
},
"values": 

[],
"label": "Syrup",
"labelForCustomer": null,
"type": null
},

{
"__type__": "MenuProdOptionSet",
"optTemplate": {
"label": "Size Option Template",
"factor": "0"
},
"template_alts": 

{
"desc": "Size",
"__type__": "ValObj"
},
"values": 

[],
"label": "Size",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Coffee and Tea",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Tea",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Gin and Tonic": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Gin and Tonic",
"price": "14.00",
"__type__": "ValObj"
},
"itemCode": "Gin and Tonic",
"onFilters": 

[
"C"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Gin Options",
"max": 4,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Gin",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Lime",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Elderflower",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Tonic",
"price": "0.00",
"__type__": "ProdOptValue"
}
],
"label": "Gin",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Cocktails",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "House Gin and Tonic",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Paper Plane": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Paper Plane",
"price": "17.00",
"__type__": "ValObj"
},
"itemCode": "Paper Plane",
"onFilters": 

[
"C"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Paper Plane Options",
"max": 4,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "High Proof Bourbon",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Aperol",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Amaro",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Lemon",
"price": "0.00",
"__type__": "ProdOptValue"
}
],
"label": "Paper",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Cocktails",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Paper Plane",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Mixers": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Mixers",
"price": "4.00",
"__type__": "ValObj"
},
"itemCode": "Mixers",
"onFilters": 

[
"N"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Mixers Options",
"max": 1,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Coca-Cola",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Ginger Ale",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Soda Water",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Tonic",
"price": "0.00",
"__type__": "ProdOptValue"
}
],
"label": "Mixers",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Juice|Shakes|Soda",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Mixers",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Mulled Wine": 

{
"__type__": "MenuProduct",
"base": {
"name": "Mulled Wine glass",
"price": "11.00",
"__type__": "ValObj"
},
"itemCode": "Mulled Wine",
"onFilters": 

[
"W"
],
"options": 

[],
"dept": null,
"foodtype": "Wine",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Mulled Wine glass",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Milkshake": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Milkshake",
"price": "6.50",
"__type__": "ValObj"
},
"itemCode": "Milkshake",
"onFilters": 

[
"N"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Milkshake Options",
"max": 3,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Hazelnut Choc",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Passionfruit",
"price": "0.00",
"__type__": "ProdOptValue"
},

{
"name": "Vanilla",
"price": "0.00",
"__type__": "ProdOptValue"
}
],
"label": "Milkshake",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Juice|Shakes|Soda",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Milkshake",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
},
"Charlatan Marlborough": 

{
"__type__": "MenuProduct",
"base": 

{
"name": "Charlatan Marlborough",
"price": "0.00",
"__type__": "ValObj"
},
"itemCode": "Charlatan Marlborough",
"onFilters": 

[
"W"
],
"options": 

[

{
"__type__": "MenuProdOptionSet",
"optTemplate": 

{
"label": "",
"factor": "0"
},
"template_alts": 

{
"desc": "Charlatan Options",
"max": 1,
"min": 1,
"__type__": "ValObj"
},
"values": 

[

{
"name": "Glass",
"price": "9.00",
"__type__": "ProdOptValue"
},

{
"name": "Can 355ml",
"price": "16.00",
"__type__": "ProdOptValue"
}
],
"label": "Charlatan",
"labelForCustomer": null,
"type": null
}
],
"dept": null,
"foodtype": "Wine",
"includedOnbanquet": "",
"itemDesc": "",
"itemLongDesc": "Charlatan Marlborough Sauvignon Blanc",
"itemMasterCode": "",
"itemType": "",
"numOfSizes": null,
"sortOrder": null,
"showLoyalty": null,
"store": 

{
"id": 0,
"stb": "1064"
}
}
},
"optTemplates": 

{
"Milk Option Template": 

{
"__type__": "MenuOptTemplate",
"desc": "Milk Options",
"label": "Milk Option Template",
"min": 1,
"max": 1,
"values": 

[

{
"name": "Soy",
"price": "0.500",
"__type__": "ValObj"
},

{
"name": "Almond",
"price": "0.500",
"__type__": "ValObj"
},

{
"name": "Coconut",
"price": "0.500",
"__type__": "ValObj"
}
],
"labelForCustomer": "",
"showLoyalty": "",
"type": "s",
"store": 

{
"stb": "1064"
}
},
"Syrup Option Template": 

{
"__type__": "MenuOptTemplate",
"desc": "Syrup Options",
"label": "Syrup Option Template",
"min": 1,
"max": 4,
"values": 

[

{
"name": "Vanilla",
"price": "0.500",
"__type__": "ValObj"
},

{
"name": "Caramel",
"price": "0.500",
"__type__": "ValObj"
},

{
"name": "Hazelnut",
"price": "0.500",
"__type__": "ValObj"
},

{
"name": "Almond",
"price": "0.500",
"__type__": "ValObj"
}
],
"labelForCustomer": "",
"showLoyalty": "",
"type": "s",
"store": 

{
"stb": "1064"
}
},
"Size Option Template": 

{
"__type__": "MenuOptTemplate",
"desc": "Size Options",
"label": "Size Option Template",
"min": 0,
"max": 1,
"values": 

[

{
"name": "Upsize",
"price": "0.500",
"__type__": "ValObj"
}
],
"labelForCustomer": "",
"showLoyalty": "",
"type": "s",
"store": 

{
"stb": "1064"
}
}
},
"storeBarcode": "1064",
"storeName": "THL"
}
}
    
"""
