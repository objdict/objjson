package nz.salect.objjson


data class xMenuFilter(
    val menuCode: String,
    val menuTitle: String,
    //override
    val onFilters: List<String>,
    val workArea:String ="",
    val dept:String = workArea // dept (department) replaces workArea
){
    companion object {

        fun fromJson(
            menuCode: String,
            menuTitle: String,
            //override
            onFilters: List<String>,
            workArea:String ="",
            dept:String = workArea // dept (department) replaces workArea
        ): xMenuFilter {
            // instanceFromJson looks for factory method which must be called 'fromJson'
            return xMenuFilter(menuCode, menuTitle, onFilters, workArea, dept)
        }
        fun fromJson(
            menuCode: String,
            menuTitle: String,
            //override
            onFilters: String,
            workArea:String ="",
            dept:String = workArea // dept (department) replaces workArea
        ): xMenuFilter {
            // instanceFromJson looks for factory method which must be called 'fromJson'
            return xMenuFilter(menuCode, menuTitle, listOf(onFilters), workArea, dept)
        }
    }


}
data class xMenuProduct(
    val itemCode: String,
    val base: BasicValue,
    //override
    val onFilters: List<String>,
    val options: List<MenuProductOption> = listOf()
)
data class xOptionTemplate(
    val label: String="", //, //label should become 'key' or 'code'
    val desc: String = label,
    val values: List<BasicValue>,
    val min: Int =1,
    val max: Int =1
)
data class BasicValue(
    val name: String? = null,
    val label: String? = null,
    val price: Int? = null,
    val desc: String? = null,
    val min: Int? = null,
    val max: Int? = null
)
//data
class OptionValue(
    val ownVals: BasicValue = BasicValue(),
    var templateVals: BasicValue = BasicValue()
)
data class MenuProductOption(
    val optTemplate: BasicValue,
    val template_alts: BasicValue=BasicValue(),
    val templateAlts: BasicValue = template_alts,
    //override
    val values: MutableList<OptionValue> = mutableListOf()
)
data class xMenu(

    val filters: Map<String, xMenuFilter>,
    val products: Map<String, xMenuProduct>,
    val optTemplates: Map<String, xOptionTemplate>,
    val storeBarcode: String ="",
    val storeName: String ="",
    val ticket: String ="",
    val dateTime: String =""
)
data class Response(val menu: xMenu)
