package nz.salect.objjson

import org.junit.Test
import java.math.BigDecimal
import kotlin.test.assertEquals


class JVMToJsonTest {
    @Test
    fun testSimple() {
        data class Fred(val text: String)

        val txt = Fred("Freddy").objectToJson()
        assertEquals("""{"text": "Freddy"}""", txt, "simple from test")
    }

    @Test
    fun testSimpleWith2() {
        data class Fred(val name: String, val age: Int)

        val txt = Fred("Freddy", 30).objectToJson()
        val txt2 = txt.replace("30", "55")
        val fred = txt2.instanceFromJson(Fred::class)!!
        println("fdy $fred")
        assertEquals(55, fred.age, "simple from test2")
    }

    @Test
    fun testSimpleWith2Fmt() {
        data class Name(val first: String, val last: String)
        data class Fred(val name: Name, val age: Int)

        val txt = Fred(Name("Freddy", "smith"), 30).objectToJson(4)
        val output = """{"name": {"first": "Freddy",
            |      "last": "smith"
            |    },
            |  "age": 30
            |}""".trimMargin()

        assertEquals(output, txt, "simple from test2 fmt")
    }


    @Test
    fun testToJsonBigDecimal() {
        data class Sample(val bdec: BigDecimal)

        val testData = Sample("0.5".toBigDecimal())

        val res = testData.objectToJson()
        assertEquals("{\"bdec\": 0.5}", res)
    }

    @Test
    fun testWithFilter() {
        data class Sample(val a: Int, val b: Int) {
            fun objJsonExcludes() = listOf("b")
        }

        val testData = Sample(1, 2)

        val res = testData.objectToJson()
        assertEquals("""{"a": 1}""", res)
    }

    @Test
    fun testNonDataClassNonValParm() {
        class Sample(val a: Int, val b: Int, c: Int) {
            fun objJsonExcludes() = listOf("b")
        }

        val testData = Sample(1, 2, 3)

        val res = testData.objectToJson()
        assertEquals("""{"a": 1}""", res)
    }

    @Test
    fun testNonDataWithExtras() {
        class Sample(val a: Int, val b: Int, c: Int) {
            val cx = c
            fun objJsonExcludes() = listOf("b")
            fun objJsonExtras() = mapOf("a" to 7, "b" to cx)
        }

        val testData = Sample(1, 2, 3)

        val res = testData.objectToJson()
        assertEquals("""{"a": 7, "b": 3}""", res)
    }

    @Test
    fun testWithNullValuesAndExtras() {
        class Sample(val a: Int?, val b: Int? = null, c: Int = 2) {
            val cx = c
            //fun objJsonExcludes() = listOf("b")
            fun objJsonExtras() = mapOf("c" to cx)
        }

        val testData = Sample(null, null, 3)

        val res = testData.objectToJson()
        assertEquals("""{"a": null, "c": 3}""", res)
    }


}
