package nz.salect.objjson


//import nz.salect.objjson.JVMTest.StudentWithFactory.Companion.fromJson2
//import jdk.nashorn.internal.runtime.regexp.joni.exception.Exception
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import java.math.BigDecimal
import kotlin.reflect.KClass
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.Test
import kotlin.test.assertNotEquals

typealias AnyMap = Map<String, Any>


val studentStrs = listOf(
    """
    {
        "name":"Tom",
        "age":18,
        "major": {
            "title": "Deep learning",
            "length": 6,
            "teacher": {
                "name":"Jim",
                "age":45,
                "book": {
                    "name": "Tom & Jim",
                    "author": "Sam",
                }
            }
        }
    }""".trimIndent(),
    """
    {
        "__type__":Student,
        "name":"Tom",
        "age":18,
        "major": {
            "__type__":Lesson,
            "title": "Deep learning",
            "length": 6,
            "teacher": {
                "__type__":Teacher,
                "name":"Jim",
                "age":45,
                "book": {
                    "__type__":Book,
                    "name": "Tom & Jim",
                    "author": "Sam",
                }
            }
        }
    }""".trimIndent(),
    """
    {
        "name":"Tom",
        "age":[88],
        "major": {
            "title": "Deep learning",
            "length": 6,
            "teacher": {
                "name":"Jim",
                "age":45,
                "book": {
                    "name": "Tom & Jim",
                    "author": "Sam",
                }
            }
        }
    }"""
)


class JVMFromJsonTest {


    @Test
    fun testClsParse() {
        data class Student(val name: String?, val age: Int?)

        val dc = studentStrs[0].instanceFromJson(Student::class)
        when (dc) {
            is Student -> {
                assertEquals(dc.name, "Tom")
                assertEquals(dc.age, 18)
            }
            else -> assertTrue(false, "oops- where is dataclass?")
        }

    }

    @Test
    fun testClsParseBD() {
        data class Student(val name: String, val age: BigDecimal)

        val dc = studentStrs[0].instanceFromJson(Student::class,raises = true)
        when (dc) {
            is Student -> {
                assertEquals(dc.name, "Tom")
                assertEquals(dc.age, 18.toBigDecimal())
            }
            else -> assertTrue(false, "oops- where is dataclass?")
        }

    }

    @Test
    fun testObjWithDefault() {
        data class Student(val name: String, val years: Int = 15)

        val dc = studentStrs[0].instanceFromJson(Student::class)!!
        assertEquals(dc.name, "Tom")
        assertEquals(dc.years, 15, "studentStr has age, not 'years' so get default")
    }

    data class StudentWithFactory(val name: String, val years: Int = 15) {

        companion object {

            fun fromJson(name: String = "", age: Int = 7): StudentWithFactory {
                // instanceFromJson looks for factory method which must be called 'fromJson'
                return StudentWithFactory("Bill", years = age)
            }
            fun fromJson(name: String = "", age: List<Int>): StudentWithFactory {
                // instanceFromJson looks for factory method which must be called 'fromJson'
                return StudentWithFactory("Bill", years = age.firstOrNull() ?: 12)
            }
        }
    }

    @Test
    fun testWithFactory() {

        val dc = studentStrs[0].instanceFromJson(StudentWithFactory::class)!!
        assertEquals("Bill", dc.name, "factory name test")
        assertEquals(18, dc.years, "studentStr has age, not 'years' but factory maps age to years")
        val dc2 = studentStrs[2].instanceFromJson(StudentWithFactory::class)!!
        assertEquals(88, dc2.years, "studentStr has age, not 'years' but factory maps age to years")
    }

    @Test
    fun testTwoLevelObj() {
        data class Lesson(val title: String, val length: Int)
        data class Student(val name: String, val age: Int, val major: Lesson)


        val studentStr = studentStrs[0]
            val student = studentStr.instanceFromJson(Student::class)!!
            assertEquals("Tom", student.name)
            assertEquals(18, student.age)

            assertEquals("Deep learning", student.major.title)
            assertEquals(6, student.major.length)

    }

    @Test
    fun testThreeLevelObj() {
        data class Teacher(val name: String, val age: Int)
        data class Lesson(val title: String, val length: Int, val teacher: Teacher)
        data class Student(val name: String, val age: Int, val major: Lesson)


        val student = studentStrs[0].instanceFromJson(Student::class)!!
        assertEquals("Tom", student.name)
        assertEquals(18, student.age)

        assertEquals("Deep learning", student.major.title)
        assertEquals(6, student.major.length)

        assertEquals("Jim", student.major.teacher.name)
        assertEquals(45, student.major.teacher.age)
    }

    @Test
    fun testFourLevelObj() {
        data class Book(val name: String, val author: String)
        data class Teacher(val name: String, val age: Int, val book: Book)
        data class Lesson(val title: String, val length: Int, val teacher: Teacher)
        data class Student(val name: String, val age: Int, val major: Lesson)


        val student = studentStrs[0].instanceFromJson(Student::class)!!
        assertEquals("Tom", student.name)
        assertEquals(18, student.age)

        assertEquals("Deep learning", student.major.title)
        assertEquals(6, student.major.length)

        assertEquals("Jim", student.major.teacher.name)
        assertEquals(45, student.major.teacher.age)

        assertEquals("Tom & Jim", student.major.teacher.book.name)
        assertEquals("Sam", student.major.teacher.book.author)
    }

    @Test
    fun testObjWithListProperty() {
        data class Student(val name: String, val count: List<Int>, val extra:String="")

        val studentStr = """
            {
                "__type__":Student,
                "name":"tom",
                "count": [1,2,3,4,5]
            }""".trimIndent()


        val student = studentStr.instanceFromJson(Student::class)!!
        assertEquals(student.name, "tom")

        assertEquals(listOf(1, 2, 3, 4, 5), student.count)
    }

    @Test
    fun testListOfObj() {
        data class Lesson(val title: String, val length: Int)
        data class Student(val name: String, val age: Int, val lessons: List<Lesson>)

        val studentStr = """
            {
                "__type__":Student,
                "name":"tom",
                "age":18,
                "lessons": [
                    {
                        "__type__":Lesson,
                        "title": "Deep Learning",
                        "length": 6
                    },
                    {
                        "__type__":Lesson,
                        "title": "Gradient Descent",
                        "length": 3
                    }
                ]
            }""".trimIndent()


        val student = studentStr.instanceFromJson(Student::class)!!
        assertEquals("tom", student.name)
        assertEquals(18, student.age)

        assertEquals(2, student.lessons.size)
        assertEquals("Deep Learning", student.lessons[0].title)
        assertEquals(6, student.lessons[0].length)
        assertEquals("Gradient Descent", student.lessons[1].title)
        assertEquals(3, student.lessons[1].length)
    }

    @Test
    fun testMapOfObj() {
        data class Lesson(val title: String, val length: Int)
        data class Student(val name: String, val age: Int, val lessons: Map<String, Lesson>)

        val studentStrs = listOf(
            """
            {
                "__type__":Student,
                "name":"tom",
                "age":18,
                "lessons": {
                    "lesson1": {
                        "__type__":"Lesson",
                        "title": "Deep Learning",
                        "length": 6
                    },
                    "lesson2": {
                        "__type__":"Lesson",
                        "title": "Gradient Descent",
                        "length": 3
                    }
                }
            }""".trimIndent(), """
                {
                "name":"tom",
                "age":18,
                "lessons": {
                    "lesson1": {
                        "title": "Deep Learning",
                        "length": 6
                    },
                    "lesson2": {
                        "title": "Gradient Descent",
                        "length": 3
                    }
                }
            }"""
        )

        for (studentStr in studentStrs) {
            val student = studentStr.instanceFromJson(Student::class)!!
            assertEquals("tom", student.name)
            assertEquals(18, student.age)

            assertEquals(2, student.lessons.keys.size)
            assertEquals(setOf("lesson1", "lesson2"), student.lessons.keys)

            assertEquals("Deep Learning", student.lessons["lesson1"]?.title)
            assertEquals(6, student.lessons["lesson1"]?.length)

            assertEquals("Gradient Descent", student.lessons["lesson2"]?.title)
            assertEquals(3, student.lessons["lesson2"]?.length)
        }

    }

    @Test
    fun testPropertyIsPlainMap() {
        data class Student(val name: String, val age: Int, val score: Map<String, Int>)

        val studentStr = """
            {
                "__type__":Student,
                "name":"tom",
                "age":18,
                "score": {
                    "lesson1": 99,
                    "lesson2": 100
                }
            }""".trimIndent()


        val student = studentStr.instanceFromJsonWithType(Student::class)
        assertEquals("tom", student.name)
        assertEquals(18, student.age)

        assertEquals(2, student.score.keys.size)
        assertEquals(setOf("lesson1", "lesson2"), student.score.keys)
        assertEquals(99, student.score["lesson1"])
        assertEquals(100, student.score["lesson2"])
    }
    //vvvvv
    @Test
    fun testHasPropertyIsNull() {
        data class Lesson(val title: String, val length: Int)
        data class Student(val name: String, val lesson: Lesson?)
        // type of Filters is AnyMap... does it still work as map?
        val testStr = """   {  name: Fred, lesson: null } """


        val test = testStr.instanceFromJson(Student::class)
        test?.let {
            assertEquals(null, test.lesson, "null property has funny value")
            //assertEquals("aaa56", menu.ticket_num)
        }
        assertNotEquals(null,test," has property no parse")
    }
    //^^^^^^^

    data class MenuObject(
        val filters: AnyMap

    ) {
        var topLevelMenu = ""
    }

    data class MenuResponse(
        val rc: Int = 0,
        val ticket_num: String = "",
        val menu: MenuObject? = null
    ) {
        var dateTime: Int = 0  // make a real dt
    }

    @Test
    fun testUsingMenu() {
        // type of Filters is AnyMap... does it still work as map?
        val testStr = """
            {

  "menu": {
    "filters": {
      "A": 3
      }
    }
}

        """.trimIndent()

        val menu = testStr.instanceFromJson(MenuResponse::class)?.menu
        //assertEquals(0, menu.rc, "Typed alias memu")
        //assertEquals("aaa56", menu.ticket_num)
    }

    @Test
    fun testMenu() {
        val rawmenu = menuRawStr.instanceFromJson()
        val menu = menuRawStr.instanceFromJson(Response::class)
        println("menu is $menu")

    }

}

class FromJsonUsingInterfaces{
    interface LessonInterface {
        val title: String;
        val length: Int
    }

    data class ILesson(override val title: String, override val length: Int) : LessonInterface {

    }

    data class IStudent(val name: String, val age: Int, val major: LessonInterface) {
        companion object {
            fun fromJsonInterfaces() = mapOf<String, List<KClass<*>>>("LessonInterface" to listOf(ILesson::class))
        }
    }

    @Test
    fun testUsingInterface() {


        val studentStr = studentStrs[0]
        val student = studentStr.instanceFromJson(IStudent::class, raises = true)!!
        assertEquals("Tom", student.name)
        assertEquals(18, student.age)

        assertEquals("Deep learning", student.major.title)
        assertEquals(6, student.major.length)


    }

    interface Duck {
        val duck: Int
    }

    class Mallard(override val duck: Int) : Duck {

    }

    data class DuckHolder(val data: List<Duck>) {
        companion object {
            fun fromJsonInterfaces() = mapOf<String, List<KClass<*>>>("Duck" to listOf(Mallard::class))
        }
    }

    @Test
    fun testWithIFInList() {
        val tdata = """{
            | data: [{duck: 1},{duck:2}]
            |}""".trimMargin()
        val duck = tdata.instanceFromJson(DuckHolder::class, raises = true)
        duck?.also {
            assertEquals(1, it.data[0].duck, "ducks not in line")
        } ?: throw(Exception("no ducks"))

    }
}


data class WithNameMapOldToNew(var newName:Int){
    companion object{
        fun fromJsonNameMap() = mapOf("oldname" to "newName")
    }
}
sealed class AgeData{
    class IntAge(val num: Int):AgeData()
}
object ProcessingSealedClasses: Spek({
    describe("when field is a class within sealed class"){
        val txt = """{ age: {num: 12}"""
        data class Person(val age: AgeData.IntAge)
        val obj = txt.instanceFromJson(Person::class)
        it("should get data"){
            assertNotEquals(null,obj,"didnt parse")
        }
        it("should have value from num in field"){
            val ageFromObj = when(val field = obj?.age){
                is AgeData.IntAge -> field.num
                else -> 0 //so does not match 12
            }
            assertEquals(12,ageFromObj,"doest get value")
        }
        val listTxt = """{ age: [{num: 12}]"""
        data class PersonList(val age: MutableList<AgeData.IntAge>)
        val objList = listTxt.instanceFromJson(PersonList::class)
        it("should get data from list"){
            assertNotEquals(null,objList,"didnt parse")
        }
        it("should have value from num in field"){
            val ageFromObj = when(val field = objList?.age?.get(0)){
                is AgeData.IntAge -> field.num
                else -> 0 //so does not match 12
            }
            assertEquals(12,ageFromObj,"doest get value")
        }

    }
})
object AdvancedFeaturesSpecJVM: Spek({

    describe("Using 'fromJsonNameMap' to allow alternate Names") {
        //val testName by memoized { testString }

        describe("companion with 'fromJsonNameMap'") {
            val dataWithMemo  by memoized{
                // is for when data used by several tests
                // so this one is just a sample
                """{ oldname: 4} """.instanceFromJson(WithNameMapOldToNew::class)
            }

            it("allows use of oldname in json") {
                val testName = """{ oldname: 4} """.instanceFromJson(WithNameMapOldToNew::class)
                testName?.also {
                    assertEquals(4, it.newName,"wrong value using nameMap")
                } ?: throw Exception("Did not parse")
            }
            it("chooses new name value when both old and new are present") {
                val testName = """{ newName: 7, oldname: 4} """.instanceFromJson(WithNameMapOldToNew::class)
                testName?.also {
                    assertEquals(7 , it.newName,"did not find newname with nameMap")
                } ?: throw Exception("Did not parse")
            }
        }
    }
})
