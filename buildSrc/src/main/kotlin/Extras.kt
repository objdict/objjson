@file:Suppress("SpellCheckingInspection")

object Vers {
    // Production/snapshot versions: add/remove "-SNAPSHOT" from string e.g. "0.26" or "0.26-SNAPSHOT"
    const val OBJJSON = "0.30.8-SNAPSHOT"

    // Jetbrains
    const val KOTLIN = "1.3.50"

    // Others
    const val NEXUS_STAGING = "0.20.0"
}

object Repos {
    const val KOTLIN_EAP = "https://dl.bintray.com/kotlin/kotlin-eap"
    // Publishing Repos
    const val RELEASE_URL = "https://oss.sonatype.org/service/local/staging/deploy/maven2/"
    const val SNAPSHOTS_URL = "https://oss.sonatype.org/content/repositories/snapshots/"
}

object Pom {
    // Required for publishing to Maven
    const val GROUP_ID = "nz.salect.objjson"
    const val ARTIFACT_ID = "objjson"
    const val VERSION = Vers.OBJJSON

    // Other relevant properties
    const val NAME = ARTIFACT_ID
    const val DESCRIPTION = "A JSON serialization library fully implemented in Kotlin"
    const val URL = "https://bitbucket.org/objdict/objjson"
    const val LICENCE_NAME = "The Apache Software License, Version 2.0"
    const val LICENCE_URL = "http://www.apache.org/licenses/LICENSE-2.0.txt"
    const val LICENCE_DISTRO = "repo"
    const val DEVELOPER_1_ID = "innov8ian"
    const val DEVELOPER_1_NAME = "Ian Ogilvy"
    const val DEVELOPER_1_EMAIL = "ian.ogilvy@salect.co.nz"
    const val DEVELOPER_1_ORG = "Salect NZ"
    const val DEVELOPER_1_ORG_URL = "https://www.salect.co.nz"
    const val DEVELOPER_2_ID = "salt_mike"
    const val DEVELOPER_2_NAME = "Michael Ball"
    const val DEVELOPER_2_EMAIL = "michael.ball@salect.co.nz"
    const val DEVELOPER_2_ORG = "Salect NZ"
    const val DEVELOPER_2_ORG_URL = "https://www.salect.co.nz"
    const val ORGANISATION_NAME = "Salect NZ"
    const val ORGANISATION_URL = "https://www.salect.co.nz"
    const val SCM_URL = "https://bitbucket.org/objdict/objjson"
    const val SCM_CONNECTION = "scm:https://bitbucket.org/objdict/objjson.git"
    const val SCM_DEV_CONNECTION = "scm:git://bitbucket.org/objdict/objjson.git"
    const val ISSUE_SYSTEM = "Bitbucket"
    const val ISSUE_URL = "https://bitbucket.org/objdict/objjson/issues"
}

object Nexus {
    const val PACKAGE_GROUP = "nz.salect"
    const val STAGING_PROFILE_ID = "2150c838713cdf"
    const val NUMBER_OF_RETRIES = 20
    const val DELAY_BTW_RETRIES_IN_MILIS = 4000
}
