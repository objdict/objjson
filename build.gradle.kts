@file:Suppress("SpellCheckingInspection")

import groovy.util.Node

// Plugins
plugins {
    kotlin("multiplatform").version(Vers.KOTLIN)
    java
    `java-library`
    `maven-publish`
    signing
    id("io.codearte.nexus-staging").version(Vers.NEXUS_STAGING)
    //id("org.decembrist.kotlin2js.reflection") version "0.1.0-beta-1"
}

repositories {
    mavenCentral()
}

//kotlin2JsReflection {
//    generatedSourcesDir = file("${project.buildDir}/generated/decembrist")
//}
// Required by artifacts
group = Pom.GROUP_ID
version = Pom.VERSION

// Kotlin Multiplatform Targets
kotlin {
    jvm()
    js()
    // For ARM, should be changed to iosArm32 or iosArm64
    iosX64("ios") {
        // The kotlin-multiplaform plugin doesn't create any production binaries by default. The only binary available
        // by default is a debug executable allowing one to run tests from the test compilation.
        // This original groovy code:
        // compilations.main.outputKinds("framework")
        // can be converted to:
        // compilations["main"].outputKinds("framework")
        // BUT "outputKinds" is DEPRECATED and should be converted to using "binaries"
        // See https://kotlinlang.org/docs/reference/building-mpp-with-gradle.html#building-final-native-binaries
        binaries {
            //framework {
            //    // Will create two executable binaries: debug and release
            //    "SharedCode"
            //}
            framework()
        }
    }
    // For Linux, should be changed to e.g. linuxX64
    // For MacOS, should be changed to e.g. macosX64
    // For Windows, should be changed to e.g. mingwX64
    mingwX64("win")


    sourceSets {
        val spekVersion = "2.0.8"
        val spekDsl = "org.spekframework.spek2:spek-dsl"
        val spekRunner = "org.spekframework.spek2:spek-runner-junit5:$spekVersion"

        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation(kotlin("reflect"))
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))

                implementation("$spekDsl-metadata:$spekVersion")
                runtimeOnly(spekRunner)
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("reflect"))
                //  org.jetbrains.kotlinx kotlinx.reflect.lite
                //  <version>1.0.0</version> </dependency>

            }
        }
        val jvmTest by getting{
            dependencies {
                val spekVersion = "2.0.7"
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
                implementation("$spekDsl-jvm:$spekVersion")
                runtimeOnly(spekRunner)
            }
        }
        val jsMain by getting {
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation(kotlin("reflect"))
                implementation("org.decembrist:kotlin2js-reflection-api:0.1.0-beta-1")

            }
        }
        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
        // iOS
        val iosMain by getting {
        }
        val iosTest by getting {
        }

    }
}

// Publishing

publishing {
    repositories {
        maven {
            val releasesRepoUrl = Repos.RELEASE_URL
            val snapshotsRepoUrl = Repos.SNAPSHOTS_URL
//            url = uri(if (Vers.OBJJSON.endsWith("SNAPSHOT")) snapshotsRepoUrl else releasesRepoUrl)
            credentials {
                username = NexusKeys.username
                password = NexusKeys.password
            }
        }
    }
}

//// Add a Javadoc JAR to each publication as required by Maven Central
val javadocJar by tasks.creating(Jar::class) {
    from(tasks.getByName("javadoc"))
    this.archiveClassifier.set("javadoc")
}

publishing {
    publications.withType<MavenPublication>().all {
        artifact(javadocJar)
    }
}

//// The root publication also needs a sources JAR as it does not have one by default
val sourcesJar by tasks.creating(Jar::class) {
    archiveClassifier.value("sources")
}

publishing.publications.withType<MavenPublication>().getByName("kotlinMultiplatform").artifact(sourcesJar)

//// Customize the POMs - add the content required by Maven Central
fun customizeForMavenCentral(pom: org.gradle.api.publish.maven.MavenPom) = pom.withXml {
    fun Node.add(key: String, value: String) {
        appendNode(key).setValue(value)
    }

    fun Node.node(key: String, content: Node.() -> Unit) {
        appendNode(key).also(content)
    }

    asNode().run {
        add("name", Pom.NAME)
        add("description", Pom.DESCRIPTION)
        add("url", Pom.URL)
        node("licenses") {
            node("license") {
                add("name", Pom.LICENCE_NAME)
                add("url", Pom.LICENCE_URL)
                add("distribution", Pom.LICENCE_DISTRO)
            }
        }
        node("developers") {
            node("developer") {
                add("id", Pom.DEVELOPER_1_ID)
                add("name", Pom.DEVELOPER_1_NAME)
                add("email", Pom.DEVELOPER_1_EMAIL)
                add("organization", Pom.DEVELOPER_1_ORG)
                //add("organizationUrl", Pom.Developer1OrganisationUrl)
            }
            node("developer") {
                add("id", Pom.DEVELOPER_2_ID)
                add("name", Pom.DEVELOPER_2_NAME)
                add("email", Pom.DEVELOPER_2_EMAIL)
                add("organization", Pom.DEVELOPER_2_ORG)
                //add("organizationUrl", Pom.Developer2OrganisationUrl)
            }
        }
        node("organization") {
            add("name", Pom.ORGANISATION_NAME)
            add("url", Pom.ORGANISATION_URL)
        }
        node("issueManagement") {
            add("system", Pom.ISSUE_SYSTEM)
            add("url", Pom.ISSUE_URL)
        }
        node("scm") {
            add("url", Pom.SCM_URL)
            add("connection", Pom.SCM_CONNECTION)
            add("developerConnection", Pom.SCM_DEV_CONNECTION)
        }
    }
}

publishing {
    publications.withType<MavenPublication>().all {
        customizeForMavenCentral(pom)
    }
}
// Sign the publications:

////// Requires signing.keyId, signing.password, and signing.secretKeyRingFile are provided as gradle.properties.

////// No complex signing configuration is required here, as the signing plugin interoperates with maven-publish
////// and can simply add the signature files directly to the publications:

signing {
    // Sign all publications
    sign(publishing.publications)
}

// Gradle Nexus Staging Plugin Configuration
nexusStaging {
    packageGroup = Nexus.PACKAGE_GROUP
    stagingProfileId = Nexus.STAGING_PROFILE_ID
    numberOfRetries = Nexus.NUMBER_OF_RETRIES
    delayBetweenRetriesInMillis = Nexus.DELAY_BTW_RETRIES_IN_MILIS
}
