objJSON
=======


 - `ObjJSON Goals`_.
 - `API.`_
 - `Solutions to Preserving Types with JSON`_.

objJSON Goals
-------------
ObjJSON is a package designed to facilitate sending and receiving
data in JSON form between client and server applications.
As opposoed to object serialisation, where the Objects take centre
stage and the JSON (or other serial form) is secondary,  ObjJSON
is designed for the case where the messages are primary, and Objects
are created to process the messages.
The goal is that messages to be sent can be describe and composed
as objects, and those object automatically produce the desired JSON
messages.

For code handling messages, processing incomming messages is more
complex than building messages to send.  With outgoing messages, the
code only handles the message your application sends and the relevant
fields within those messages.  With incomming messages, the application
must determine which message is received and deal with all possible
messages, including unexpected
variations of messages.

A further complication is 'version tolerance'. The other end of the
commications link could be still using an older version of message
format, or may be already updated to a newer version, as it may be
impractical to update both ends of the system at once.

ObjJson is specifically designed to allow extracting the currently
needed data from a message in order to implement the currently defined
application, and allow ignoring data not relevant to
the current application.  This is a different goal than systems which
target imposing the strictest possible rules on the Json data, or at
the other extreme, seek to adjust their functionality to the data.

The result is that if an message is described as, for example,
containg a person object that contains a name object, then objJson
will attempt to read that data from the message, and if data arrives
describing instead a name object at the outer level and a person object
at the inner level, there will be no attempt by objJson to adjust to
this different structure. ObjJson reads expected data only provide
that data complies with the defined object heirachy.

Futher note that when communicating between systems, even beyond the
needs of version tolerence, different systems may have variations of
the defined object properties or hierarchy and also be written in
different languages. In other words, flexibility is important.

API.
----

 - `From Json String to Map`_.
 - `From Map to Object`_.
 - `From Json String to Object`_.

From Json String to Map
~~~~~~~~~~~~~~~~~~~~~~~

    """{ "abc": 3 }""".instanceFromJson()

`instanceFromJson` as a string extension with no parameters in the
braces, Will decode a json string into a map.
Currently, numeric values (numbers not enclosed in quotes)
become either 'Int' or if they contain a decimal, BigDecimal.
Another limitation currently is that the parser only functions if
the outer level is an object. Lists, or simple values, at the
outer level are legal JSON, but not currently handled.  This has
happened as current usage simply does not call for top level being
other than an object, but the support will likely be added in future.

From Map to Object
~~~~~~~~~~~~~~~~~~
example::

   data class Person(val name:String, val age:Int=18)
   mapOf("name" to "Tom").instanceFromMap(Person::class)

`instanceFromMap` takes the data from a map to provide the
values to the constructor of an object.

So in the above example, a Person object would be returned, with
the 'name' of 'Tom' and the default age of 18, as age was not present
in the map.  Far more complext cases with nested object can also
be used, but these are discussed in `From JSON string to Object`_
below. The main use case for calling 'instanceFromMap' is where an
object

From JSON String to Object
~~~~~~~~~~~~~~~~~~~~~~~~~~
The central function of ObjJson is to read json data and instance
a desired object hierarchy. So the above Map to Object example
becomes::

   data class Person(val name:String, val age:Int=18)
   """{"name" : "Tom" }""".instanceFromJson(Person::class)

Relaxed Syntax
++++++++++++++
ObjJson allows for relaxed Json syntax. All string values that
do not contain whitespace characters, can omit the " characters,
so the second line above could also be simply::

   "{ name : Tom }".instanceFromJson(Person::class)

Nested Objects
++++++++++++++
A two level example with object nesting would be::

   data class FullName(val first:String, val last:String="theFamousOne")
   data class Person(val fullname:FullName, val age:Int=18)
   "{ fullname: { first: Cher }}".instanceFromJson(Person::class)

Factory Constructors
++++++++++++++++++++
Previous examples use the class primary constructor, together
with values from the map/json data, to instance objects.  However
a class can also have a factory method 'fromJson', which if defined
will be called in place of the primary constructor.
The primary constructor can be restrictive, as keys in the map/json
must match exactly with the names of fields in the class.
In the example under `Nested Objects`_ the first key 'fullname' and
the name of the field in the person object 'name' must match, as
must the key 'first' and the field 'first' in the 'FullName' object.

Using the primary construct is very inflexible as even calling the
field 'fullName' as is the custom in Kotlin, would result in the value
of the name not being found in the json data, since the key does not
match.  Data in the format of as used in `Relaxed Syntax`_, where name
is just a string could not be processed. What if the old version was
name as a string, and the new has name as an object, and a server
has to be able to process data arriving from both older apps not yet
updated having name as a string, together with newer apps where fullName
is an object?::

    data class FullName(val first:String, val last:String="theFamousOne")
    data class Person(val fullName:FullName, val age:Int=18){
       companion object{
         fun fromJson(name:String?, fullname:FullName?, age:Int=18)
            =  Person(when{
                       fullName!=null -> fullname
                       name!=null-> FullName(name)
                       else-> "no name!"
                     },age)
    }
    "{ fullname: { first: Cher }}".instanceFromJson(Person::class)
    "{ name : Tom }".instanceFromJson(Person::class)

The Person class gets to have the fullName with a capitol N, despite
the data never sending the name in this form, and can handle both the
old and the new format for the name.

Note that the factory method (as is normal for a factory method) must
be defined on the companion object, as a regular method would need
an object to be provided in order to call the factory.



`The First Problem with JSON Data: Types`_
------------------------------------------

See the detailed explanation of types below, but suffice to say
there is insufficient information in JSON notation to recover all
types, at least the types needed by many applications.
The problem is not very significant for a dynamically typed
language such as Javascript, becomes more significant for a language
such as Python, and becomes quite serious for a statically typed
language such a Kotlin, where even an Int and a Long are different types,
even though the look very much the same.

`A More Significant Problem with JSON Data: Bottom up Types`_
-------------------------------------------------------------

A fundamental premise of most Json decoding, is that the type can
be inferred from the data. This allows the information decoded from
JSON to potentially contain any types, in any arrangement.

Very flexible, and applications that pass the data through are fine
with this, but what about appling the data to classes?
Consider a class definition
in Kotlin. It has specific pre-determined fields, with predetermined
types.  Unless the types are reduced to 'Any' and then cast with case
statements (which would become labourious), the data can only be in
the form of the required type.

The solution is 'top-down types' or driving the type from the class
receiving the data, and looking inside that class for classes within the data.


`The Problem with Types: Background`_
-------------------------------------


JSON comes from Javascript, and javascript is a dynamically typed language where objects can be
considered as collections (a map/dictionary) of the
fields of the object and the class (being dynamic)of the object is not as important as with other languages.
So a Person object with a two fields, "name" and "age", can be considered the same as a map/dictionary with
a value for each of the keys "name" and "age".  When a language such as python dumps such a person class to
JSON and then reads the class back, what is read back is now no longer a class but a dictionary.  In kotlin
a class dumped and read back becomes a map.  The original type is lost.

Now consider how two variables, one is a decimal 3.4, the other a float of 4.3 will appear in JSON.
Again, there is nothing to indicate which value is of which type.  In general, dumping to JSON and then
reading the data back will result in a lose type information for a significant number of data types.


Consider some sample data.  An 'Person' object that contains a 'Name' object.

Defined in kotlin as::

    data class Person(val name: Name, val age:Int, val height: BigDecimal, val born: Date, val toys:List<String>)
    data class Name(val first: String, last:String)

An example dumped to javascript this would be::

    { "name" : { "first": "johnny", "last": "smith" }, "age": 3, "height": 0.95,
        "born": "2016/03/01", "toys":[]
    }

JSON does not actually have a date, but the example above follows the javascript way of including a
date and is the most widely recommended way.

Reading this back to python or kotlin with a simply JSON reader has the following problems:

The overall data type is a map/dictionary, not a Person, and the same with 'name' which is again a
map/dictionary instead of a Name object.

The name object is not only a map(Kotlin) or dictionary (Python) but must be a map of type 'Any'
since it is not clear what types would be allowed.

Should age be a Long, Int, Short or Byte?  This is not a problem with Python as one int type covers
every case, so is really a Kotlin specific problem.

It is unclear if "age" should be a float or Decimal (Python) or Float, Double or BigDecimal (Kotlin).

While "toys" should be a list, in Kotlin it is necessary to know what type of list.

The date in "born" could be a date, or could be a string that just happens to contain a date. What type
should the decoding code decide?

The problem is that there is no system designed to preserve type. With a dynamically typed language like
javascript, does that matter?   There are clear cases where that matters with Python, and even more
cases where it matters with Kotlin.

What is required is a system for instancing objects from JSON data that preserves the original types.
The complexity
comes when looking at JSON with several levels of nested {} blocks.  Which blocks represent which objects, and
which blocks are actually maps or dictionaries?

move below to bottom up.
It can get all too hard to decode leaving code accessing from the Map
and such code is not self documenting.


But where OjbJSON gets interesting is when converting object structures to JSON and back.


Solutions to Preserving Types with JSON
---------------------------------------

ObjDict is a python library designed around dealing with the limitations of JSON in a Python environment.
Four main steps are provided for:
Enhancements to objects to allow control over representation of the object as JSON data
Addition of __type__ annotations to JSON files to allow indicating type
The ObjDict type allowing
specifically decoding objects within the overall returned json into the
correct objects.

ObjJSON is an iteration of the same goal, but in the context of kotlin the methodology changes.


The Bottom Up Approach To Preserving Types
------------------------------------------

The first step is the ObjJSON simply provides for what many other json libraries do, it converts maps to
JSON and JSON back to maps.  This is done in a very forgiving way and json data can omit the " characters around
keys that have no embedded spaces or special characters, and around strings which need the quotes to avoid being mistaken for another data type.  A string "hello"
does not need the quotes, but "3" to be seen as a string does.

The bottom up approach is to include data in the JSON file to indicate type. This means that within a
nested structure, type information can be determined even at the lowest level of the structure.



Overview - A different Approach: Top Down
-----------------------------------------
The goal of this package is to facilitate sending and receiving of JSON data.

The goal is that for sending data, in place of defining maps, the data can be defined through
data classes.  Each data class to be used to produce JSON, requires as toJSON method.

The goal is also for Data received to able to be read into data classes.

The the most common approach is for JSON data is to read all {} constructs into maps.  This means::

   { "name": {"first": "Fred", "last": "Blogs" }, "age": 30 }  //sample json data

would be represented decoded in kotlin as::

    mapOf( "name" to mapof("first" to "Fred", "last" to "Blogs"), "age" to 30)

Manipulating such data is messy. Data types have to use 'Any' and then cast to the actual desired type.
The data type itself gives no clue as what data is expected, necessitating a separate spec of what the data
should be, and somewhat obscure code to work with this data.

Now consider::

    data class Person(val name:PersonName, val age:Int)
    data class PersonName(val first:String, val last:String)

This provides a clearer specification of the data, and if these two classes declarations can be used
by the JSON decoder,
then we enable a different approach to decoding JSON data.

Limitations of Top Down.
~~~~~~~~~~~~~~~~~~~~~~~~
The bottom up approach to decoding JSON into classes (as used in ObjDict) is work from the lowest
level. Each { } or map contains information such as a 'type' entry allowing determining
what type to use to instance an object.  So any map can become any object.  This approach has great flexibility,
because our sample
json data (above) could potentially have a 'PersonName' in any location.  There could be a PersonName object
as the data for 'age'. The bottom up approach where each item specifies its own type allows complete flexibility
to have absolutely any object in 'object vocabulary' at any location in the message.  However, processing such
flexibility requires very complex code. The normal case is that an application only desires data that conforms
to a specification, and is unable to make any sense of other data combinations. In a statically typed language,
how could it be possible to define an object structure to match a combination of data defined only at run-time?

The limitaion of top down is that data which should not be forced to conform to a predefined specification, has to be
left as a map.  There is not even an attempt to define custom types on the fly to match the data represented
in the json.  The example for our sample json data, makes no use of such
flexibility.  Normally code can handle 'Person' objects with a specific constrained set of values, with the
PersonName object also constrained by strict rules. While Json would allow another person object, or even
a list of Person objects as a value with a PersonName object, top down decoding means staying with the definition
supplied.  There are 2 choices for when data does not match the definition supplied:

 - report the data not matching the defintion as an error
 - attempt to work with the data by
      - ignore extra data or new fields as possibly for a future version, or previous version
      - use default values where possible to allow for data being omitted
      - allow for reading data from text format, rather than determining type from data to eliminate type mismatches

Objjson uses the second approach, as the main application is for messaging between applications,
and versions of sending and receiving applications may change independantly.

Reducing the messages to only predetermined defintions is in practice rarely a
restriction.
The fact is that code can usually only use data that is constrained to expectations
and values if the values are of the expected type.  Top down means less code,
less error checking and works best with static types.
There is a specific top level object, and all top level fields of the
the JSON data must follow the rules of that top level object type,  fields of the top level object can
themselves be objects with their own list of fields, and the sames at every level.

The expected type of every {} block could be decided by the overall object structure.

Challenges to the Top Down Approach
-----------------------------------
Not every message or package of JSON data automatically fits the top down approach. Consider::

    { msgType: "login", msgData: { header: "header text", userName: "fred" } }

This is json text for a received message, with data inside the message indicating the message type.  Different
message types might logically have different classes to represent their data.
The top-down approach means applying
a class structure on the overall message, and without union types, 'msgData' will be the same type for every
message.  The 'bottom up' approach of determining the type from data within the map works well for this
situation, the challenge is to provide a solution using 'top-down'.

Currently, the optimum solution thought to be to think of 'msgData' as a sealed class.
Logic to determine which case within the sealed class applies could be specified in
a lambda.... however that is currently for a future version.  Currently, the top
level object would need to be a map, and then which object to apply to that map
would be determined later.  An alternative is to define different field names
for the different message data types, so in place of 'msdData', there
could be 'loginData' and 'logoutData' or whatever data is relevant for each possible
message type, but this alternative requires that message specification is not
already set.

Changing the fields names to suit as JSON decoding system is not always desirable or even possible,
so an alternative solution is desirable, moving back to two part solution.
Decode in two steps, one step decoding
only the 'msgType' from map data, and the second step decoding the 'msgData' to the required type::

    data class Message(var msgType:String, var msgData:Map<String,Any?>)
    data class LoginData(var header:String, userName:String)

    var message = messageText.instanceFromJson(Message::class)
    var loginData = message.msgData.instanceFromMap(LoginData::class)

This code uses a two step json decoding. Step1, as a string extension, first converts the Json string
into map format, then instances an object of the specified type from the resulting map.  Step 2
is an extension to the Map<String,Any> class, and processes map data applying types.



